var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("misc/utils", ["require", "exports", "three", "misc/manager"], function (require, exports, THREE, manager_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function clamp(val, min, max) {
        return Math.min(max, Math.max(min, val));
    }
    exports.clamp = clamp;
    function get_line_intersection(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y) {
        var s1_x, s1_y, s2_x, s2_y;
        s1_x = p1_x - p0_x;
        s1_y = p1_y - p0_y;
        s2_x = p3_x - p2_x;
        s2_y = p3_y - p2_y;
        var s, t;
        s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
        t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
        if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
            return new THREE.Vector2(p0_x + (t * s1_x), p0_y + (t * s1_y));
        }
        return 0; // No collision
    }
    exports.get_line_intersection = get_line_intersection;
    function get_S(A1, B1, A2, B2, distance) {
        var d1x = B1.x - A1.x;
        var d1y = B1.y - A1.y;
        var d1z = B1.z - A1.z;
        var d2x = B2.x - A2.x;
        var d2y = B2.y - A2.y;
        var d2z = B2.z - A2.z;
        /*
        //  SAz = A1.z + ((A1.x - SAx)*d1x+(A1.y-SAy)*d1y)/d1z;
        //  SAz=  h    + ((i    - b  )*j  +(k   -SAy)*l  )/m
        //
        //  SAy = A2.y + ((A2.x - SAx)*d2x+(A2.z-SAz)*d2z)/d2y;
        //  SAy = ((A2.x-SAx)*d2x+A2.y*d2y+d2z*(A2.z-(A1.z+(A1.x-SAx)*d1x+A1.y*d1y)/d1z))/(d2y+(d1y*d2z)/d1z);
        //  SAy = ((a   - b )*c  +d   *e  +f  *(g   -(h   +(i   -b  )*j  +k   *l  )/m  ))/(e  +(l  *f  )/m  );
        //  SAy = (m  *(a   *c  -b  *c  +d   *e  )-f  *(-b  *j  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
        //var SAy = (d1z*(A2.x*d2x-SAx*d2x+A2.y*d2y)-d2z*(-SAx*d1x-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y);
     
        //  SAy = (m  *(a   *c    +d   *e  )-b*c*m+f*b*j-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
        //  SAy = (m  *(a   *c    +d   *e  )+b(-c*m+f*j)-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
        //  SAy = (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ) +b(-c*m+f*j)/(e*m+f*l)
        //  SAy =  b*(-c*m+f*j)/(e*m+f*l) + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
        //  SAy =  b *(-c  *m  +f  *j)  /(e  *m  +f  *l  ) + (m  *(a   *c   +d   *e  )-f  *(-g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
        //SAy   = SAx*(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y) + (d1z*(A2.x*d2x +A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y);
        //SAy     = SAx*KY + CY;
        */
        var KY = (-d2x * d1z + d2z * d1x) / (d2y * d1z + d2z * d1y);
        var CY = (d1z * (A2.x * d2x + A2.y * d2y) - d2z * (-A2.z * d1z + A1.z + A1.x * d1x + A1.y * d1y)) / (d2y * d1z + d2z * d1y);
        /*
        //var SAz = A1.z + ((A1.x - SAx)*d1x+(A1.y-SAx*(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y) + (d1z*(A2.x*d2x +A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y))*d1y)/d1z;
        //  SAz=  h    + ((i    - b  )*j  +(k   -b*(-c*m+f*j)/(e*m+f*l) + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l  )/m
        //SAz=  h    + (i*j    - b*j  +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l -b*(-c*m+f*j)/(e*m+f*l)*l )/m
        //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l- b*j -b*(-c*m+f*j)/(e*m+f*l)*l )/m
        //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l - b*(j+(-c*m+f*j)/(e*m+f*l)*l) )/m
        //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l )/m - b*(j+(-c*m+f*j)/(e*m+f*l)*l)/m
        //SAz=  - b*(j+(-c*m+f*j)/(e*m+f*l)*l)/m + h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l )/m;
        //SAz=-b  *(j  +(-c  *m  +f  *j  )/(e  *m  +f  *l  )*l  )/m   + h   +(i   *j  +(k   +(m  *(a   *c  +d   *e  )-f  *(-g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l  )/m;
        //SAz=-SAx*(d1x+(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y)*d1y)/d1z + A1.z+(A1.x*d1x+(A1.y+(d1z*(A2.x*d2x+A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y))*d1y)/d1z;
        //  SAz=  h    + ((i    - b  )*j  +(k   -SAy)*l  )/m
        //  SAz=  h    + ((i    - b  )*j  +(k   -b*KY+CY)*l  )/m
        //  SAz=  h    + (i*j   - b*j -b*KY*l +(k   +CY)*l  )/m
        //  SAz=  h    + (i*j   - b*(j+KY*l) +(k   +CY)*l  )/m
        //  SAz=  h    + (i*j    +(k   +CY)*l  )/m - b*(j+KY*l)/m
        //  SAz= - b*(j+KY*l)/m + h    + (i*j    +(k   +CY)*l  )/m
        //  SAz= - b*(j  +KY*l  )/m   + h    + (i   *j    +(k   +CY)*l  )/m
        //  SAz= - b*(d1x+KY*d1y)/d1z + A1.z + (A1.x*d1x  +(A1.y+CY)*d1y)/d1z
        //SAz=SAx*KZ+CZ;
        */
        var KZ = -(d1x + KY * d1y) / d1z;
        var CZ = A1.z + (A1.x * d1x + (A1.y + CY) * d1y) / d1z;
        //(A1.x-SAx)*(A1.x-SAx)          +  (A1.y-SAy)*(A1.y-SAy)         +  (A1.z-SAz)*(A1.z-SAz)
        //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  SAy*SAy-2*SAy*A1.y+A1.y*A1.y  +  SAz*SAz-2*SAz*A1.z+A1.z*A1.z
        //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  (SAx*KY+CY)*(SAx*KY+CY)-2*(SAx*KY+CY)*A1.y+A1.y*A1.y  +  (SAx*KZ+CZ)*(SAx*KZ+CZ)-2*(SAx*KZ+CZ)*A1.z+A1.z*A1.z
        //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  SAx*SAx*KY*KY + SAx*2*KY*CY + CY*CY - SAx*2*KY*A1.y + A1.y*A1.y-2*CY*A1.y+A1.y*A1.y  +  SAx*SAx*KZ*KZ + SAx*2*KZ*CZ + CZ*CZ - SAx*2*KZ*A1.z + A1.z*A1.z-2*CZ*A1.z+A1.z*A1.z
        //SAx*SAx*(1+KY*KY+KZ*KZ)  +  SAx*(-2*A1.x+2*KY*CY-2*KY*A1.y+2*KZ*CZ-2*KZ*A1.z)  +   A1.x*A1.x + CY*CY  + A1.y*A1.y-2*CY*A1.y+A1.y*A1.y + CZ*CZ  + A1.z*A1.z-2*CZ*A1.z+A1.z*A1.z - h1*h1 == 0
        //SAx*SAx*a  +  SAx*b  +   A1.x*A1.x + CY*CY  + c == 0
        var a = 1 + KY * KY + KZ * KZ;
        var b = -2 * A1.x + 2 * KY * CY - 2 * KY * A1.y + 2 * KZ * CZ - 2 * KZ * A1.z;
        var c = A1.x * A1.x + CY * CY + A1.y * A1.y - 2 * CY * A1.y + A1.y * A1.y + CZ * CZ + A1.z * A1.z - 2 * CZ * A1.z + A1.z * A1.z - distance * distance;
        var delta = b * b - 4 * a * c;
        var SAx = 0;
        if (delta < 0)
            console.log("no corresponding point");
        else if (delta == 0)
            SAx = -b * 0.5 / a;
        else
            SAx = (-b + Math.sqrt(delta)) * 0.5 / a;
        var SAy = SAx * KY + CY;
        var SAz = SAx * KZ + CZ;
        return new THREE.Vector3(SAx, SAy, SAz);
    }
    exports.get_S = get_S;
    function raycast(objects) {
        // update the picking ray with the camera and mouse position
        manager_1.Manager.raycaster.setFromCamera(manager_1.Manager.mouse, manager_1.Manager.camera);
        // calculate objects intersecting the picking ray
        return manager_1.Manager.raycaster.intersectObjects(objects);
    }
    exports.raycast = raycast;
});
define("misc/camera", ["require", "exports", "three", "misc/utils", "misc/manager"], function (require, exports, THREE, utils_1, manager_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Camera = /** @class */ (function (_super) {
        __extends(Camera, _super);
        function Camera(fov, aspect, near, far) {
            var _this = _super.call(this, fov, aspect, near, far) || this;
            _this.pitch = 0;
            _this.yaw = 0;
            return _this;
        }
        Camera.prototype.move = function (event) {
            this.pitch += event.movementY / 100.0;
            this.yaw += event.movementX / 100.0;
            this.pitch = utils_1.clamp(this.pitch, 0, Math.PI / 2.0);
            this.setCameraPos(this.pitch, this.yaw);
        };
        Camera.prototype.setCameraPos = function (pitch, yaw) {
            this.rotation.x = -pitch;
            this.position.y = Math.sin(pitch) * 3;
            this.position.z = Math.cos(pitch) * 3;
            manager_2.Manager.center.rotation.y = yaw;
        };
        return Camera;
    }(THREE.PerspectiveCamera));
    exports.Camera = Camera;
});
define("elts/line", ["require", "exports", "three"], function (require, exports, THREE) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Line = /** @class */ (function () {
        function Line(point, vector) {
            this.point = point;
            this.vector = vector;
        }
        Line.prototype.distanceToPoint = function (point) {
            var dist = new THREE.Vector3().subVectors(this.point, point).cross(this.vector).length();
            return dist;
        };
        return Line;
    }());
    exports.Line = Line;
});
define("menu/iinspectable", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("elts/surfaceMesh", ["require", "exports", "three"], function (require, exports, THREE) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SurfaceMesh = /** @class */ (function (_super) {
        __extends(SurfaceMesh, _super);
        function SurfaceMesh(geom, mat, cursor) {
            if (cursor === void 0) { cursor = "default"; }
            var _this = _super.call(this, geom, mat) || this;
            _this.cursor = cursor;
            return _this;
        }
        SurfaceMesh.prototype.get_Inspector = function () {
            return this.owner.get_Inspector();
        };
        return SurfaceMesh;
    }(THREE.Mesh));
    exports.SurfaceMesh = SurfaceMesh;
});
define("menu/colorPicker", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ColorPicker = /** @class */ (function () {
        function ColorPicker() {
            this.domElement = document.createElement("input");
            this.domElement.type = "color";
            var t = this;
            this.domElement.oninput = function () { t.oninput(); };
        }
        ColorPicker.prototype.setColor = function (color) {
            this.domElement.value = "#" + color;
        };
        ColorPicker.prototype.oninput = function () {
            this.color = this.domElement.value;
            this.colorCallback();
        };
        return ColorPicker;
    }());
    exports.ColorPicker = ColorPicker;
});
define("elts/surfaceInspector", ["require", "exports", "menu/colorPicker", "misc/manager"], function (require, exports, colorPicker_1, manager_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SurfaceInspector = /** @class */ (function () {
        function SurfaceInspector(surface) {
            this.htmlContainer = document.createElement("div");
            this.colorPicker = new colorPicker_1.ColorPicker();
            this.htmlContainer.appendChild(this.colorPicker.domElement);
            var t = this;
            var func = function (event) { surface.mesh.material.color.setStyle(t.colorPicker.color); manager_3.Manager.isDirty = true; };
            this.colorPicker.colorCallback = func;
            this.colorPicker.setColor(surface.mesh.material.color.getHexString());
        }
        return SurfaceInspector;
    }());
    exports.SurfaceInspector = SurfaceInspector;
});
define("elts/surface", ["require", "exports", "three", "elts/line", "elts/surfaceInspector"], function (require, exports, THREE, line_1, surfaceInspector_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Surface = /** @class */ (function () {
        function Surface(owner, mesh) {
            this.owner = owner;
            this.mesh = mesh;
            this.mesh.owner = this;
            this.container = new THREE.Object3D();
            this.container.add(this.mesh);
            this.subInspector = new surfaceInspector_1.SurfaceInspector(this);
        }
        Surface.prototype.getLocalPosition = function (point) {
            return new THREE.Vector3(point.x, 0, point.y);
        };
        Surface.prototype.getNormal = function () {
            console.log("Please override getNormal!");
        };
        Surface.prototype.intersectSurface = function (other) {
            var n1 = this.mesh.up.clone().transformDirection(this.mesh.matrixWorld);
            var p1 = this.mesh.localToWorld(new THREE.Vector3(0, 0, 0));
            var d1 = p1.x * n1.x + p1.y * n1.y + p1.z * n1.z;
            var n2 = other.mesh.up.clone().transformDirection(other.mesh.matrixWorld);
            var p2 = other.mesh.localToWorld(new THREE.Vector3(0, 0, 0));
            var d2 = p2.x * n2.x + p2.y * n2.y + p2.z * n2.z;
            var u = new THREE.Vector3().crossVectors(n2, n1).normalize();
            var Pu = new THREE.Vector3(0, 0, 0);
            if (Math.abs(u.z) > Math.abs(u.y) && Math.abs(u.z) > Math.abs(u.x)) {
                Pu.z = 0;
                Pu.x = (n1.y * d2 - n2.y * d1) / (n1.x * n2.y - n2.x * n1.y);
                Pu.y = (n2.x * d1 - n1.x * d2) / (n1.x * n2.y - n2.x * n1.y);
            }
            else if (Math.abs(u.y) > Math.abs(u.x)) {
                Pu.y = 0;
                Pu.x = (n1.z * d2 - n2.z * d1) / (n1.x * n2.z - n2.x * n1.z);
                Pu.z = (n2.x * d1 - n1.x * d2) / (n1.x * n2.z - n2.x * n1.z);
            }
            else {
                Pu.x = 0;
                Pu.y = (n1.z * d2 - n2.z * d1) / (n1.y * n2.z - n2.y * n1.z);
                Pu.z = (n2.y * d1 - n1.y * d2) / (n1.y * n2.z - n2.y * n1.z);
            }
            return new line_1.Line(Pu, u);
        };
        Surface.prototype.delete = function () {
        };
        Surface.prototype.get_Inspector = function () {
            return this.owner.get_Inspector();
        };
        Surface.prototype.get_SubInspector = function () {
            return new surfaceInspector_1.SurfaceInspector(this);
        };
        return Surface;
    }());
    exports.Surface = Surface;
});
define("menu/selectable", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Selectable = /** @class */ (function () {
        function Selectable() {
        }
        return Selectable;
    }());
    exports.Selectable = Selectable;
});
define("elts/jointHandle", ["require", "exports", "three", "elts/joint", "misc/utils"], function (require, exports, THREE, joint_1, utils_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var JointHandle = /** @class */ (function (_super) {
        __extends(JointHandle, _super);
        function JointHandle(geom, mat, jointOwner, cursor, handleType) {
            var _this = _super.call(this, geom, mat) || this;
            _this.jointOwner = jointOwner;
            _this.cursor = cursor;
            _this.handleType = handleType;
            joint_1.Joint.raycastArray.push(_this);
            return _this;
        }
        JointHandle.prototype.mousemove = function (event) {
            if (!this.jointOwner)
                return false;
            var intersections = utils_2.raycast([this.jointOwner.root.mesh]);
            if (intersections.length > 0) {
                var pos = this.parent.parent.worldToLocal(intersections[0].point).sub(this.initialSelectionPoint);
                this.parent.position.x = pos.x;
                this.parent.position.z = pos.z;
                this.jointOwner.updatePoints(this.handleType);
                return true;
            }
            return false;
        };
        JointHandle.prototype.selected = function (event) {
            var intersections = utils_2.raycast([this]);
            this.initialSelectionPoint = this.parent.parent.worldToLocal(intersections[0].point).sub(this.parent.position);
        };
        JointHandle.prototype.get_Inspector = function () {
            return null;
        };
        return JointHandle;
    }(THREE.Mesh));
    exports.JointHandle = JointHandle;
    (function (JointHandle) {
        var handleType;
        (function (handleType) {
            handleType[handleType["A"] = 0] = "A";
            handleType[handleType["Center"] = 1] = "Center";
            handleType[handleType["B"] = 2] = "B";
        })(handleType = JointHandle.handleType || (JointHandle.handleType = {}));
        ;
    })(JointHandle = exports.JointHandle || (exports.JointHandle = {}));
    exports.JointHandle = JointHandle;
});
define("elts/jointInspector", ["require", "exports", "elts/jointHandle"], function (require, exports, jointHandle_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var JointInspector = /** @class */ (function () {
        function JointInspector(joint) {
            this.htmlContainer = document.createElement("div");
            var coord1 = document.createElement("input");
            coord1.type = "number";
            coord1.style.maxWidth = "70px";
            //rescale to better human usage, might want to do that globally
            coord1.value = (joint.pointA.position.x * 10.0).toString();
            //workaraound hax to correctly update the rotations
            //TODO: proper fix correctly implemented!!!
            //TODO: round value getting from THREE to fit settings
            coord1.oninput = function () { if (coord1.value == "")
                return; joint.pointA.position.x = parseFloat(coord1.value.slice(0, coord1.value.indexOf(".") + 6)) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.Center); setTimeout(function () { joint.pointA.position.x = parseFloat(coord1.value) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.Center); }, 50); };
            this.htmlContainer.appendChild(coord1);
            var coord2 = document.createElement("input");
            coord2.type = "number";
            coord2.style.maxWidth = "70px";
            coord2.value = (joint.pointA.position.z * 10).toString();
            coord2.oninput = function () { if (coord2.value == "")
                return; joint.pointA.position.z = parseFloat(coord2.value) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.Center); setTimeout(function () { joint.pointA.position.z = parseFloat(coord2.value) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.Center); }, 50); };
            this.htmlContainer.appendChild(coord2);
            var dist = document.createElement("input");
            dist.type = "number";
            dist.style.maxWidth = "70px";
            dist.value = (joint.getDistance() * 10).toString();
            dist.oninput = function () { if (dist.value == "")
                return; joint.pointB.position.z = joint.pointA.position.z + parseFloat(dist.value) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.B); setTimeout(function () { joint.pointB.position.z = joint.pointA.position.z + parseFloat(dist.value) / 10.0; joint.updatePoints(jointHandle_1.JointHandle.handleType.B); }); };
            this.htmlContainer.appendChild(dist);
            //TODO: fill back in 0s if specified by user
            //TODO:prevent too big values BUT make it an option (both sides actually (nums after .))
            joint.onchange.push(function (joint) { coord1.value = (Math.round(joint.pointA.position.x * 10.0 * 100000) / 100000).toString(); coord2.value = (joint.pointA.position.z * 10.0).toString(); dist.value = (joint.getDistance() * 10).toString(); });
        }
        return JointInspector;
    }());
    exports.JointInspector = JointInspector;
});
define("elts/joint", ["require", "exports", "three", "elts/jointHandle", "elts/jointInspector", "misc/manager"], function (require, exports, THREE, jointHandle_2, jointInspector_1, manager_4) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Joint = /** @class */ (function () {
        function Joint(surface, pointOne, pointTwo) {
            this.onchange = new Array();
            this.pointOne = pointOne;
            this.pointTwo = pointTwo;
            this.pointA = new THREE.Object3D();
            this.pointB = new THREE.Object3D();
            this.setSurface(surface);
            var geometry = new THREE.SphereGeometry(0.025, 0.32, 0.32);
            var material = new THREE.MeshBasicMaterial({ color: 0x222222 });
            this.sphereA = new jointHandle_2.JointHandle(geometry, material, this, "move", jointHandle_2.JointHandle.handleType.A);
            this.pointA.add(this.sphereA);
            this.sphereB = new jointHandle_2.JointHandle(geometry, material, this, "move", jointHandle_2.JointHandle.handleType.B);
            this.pointB.add(this.sphereB);
            var dist = pointOne.distanceTo(pointTwo);
            var geometryCyl = new THREE.CylinderGeometry(0.0125, 0.0125, 1, 32);
            geometryCyl.translate(0, -0.5, 0);
            this.cylinder = new jointHandle_2.JointHandle(geometryCyl, material, this, "pointer", jointHandle_2.JointHandle.handleType.Center);
            this.cylinder.name = "cyl";
            this.cylinder.rotation.x = -Math.PI / 2.0;
            this.cylinder.scale.set(1, dist, 1);
            this.pointA.add(this.cylinder);
            this.subInspector = new jointInspector_1.JointInspector(this);
        }
        Joint.prototype.setSurface = function (surface) {
            this.root = surface;
            this.root.container.add(this.pointA);
            this.root.container.add(this.pointB);
            this.setPoints();
        };
        Joint.prototype.setPoints = function () {
            var pos = this.root.getLocalPosition(this.pointOne);
            this.pointA.position.set(pos.x, pos.y, pos.z);
            var pos2 = this.root.getLocalPosition(this.pointTwo);
            this.pointB.position.set(pos2.x, pos2.y, pos2.z);
            this.change();
            //update cylinder
        };
        Joint.prototype.updatePoints = function (type) {
            this.changed = type;
            this.leaf.owner.updatePoints(this);
            this.change();
        };
        Joint.prototype.getDistance = function () {
            return this.pointA.position.distanceTo(this.pointB.position);
        };
        Joint.prototype.delete = function () {
            this.pointA.parent.remove(this.pointA);
            this.pointB.parent.remove(this.pointB);
        };
        Joint.prototype.get_SubInspector = function () {
            return this.subInspector;
        };
        Joint.prototype.change = function () {
            var _this = this;
            this.onchange.forEach(function (callback) {
                callback(_this);
            });
            manager_4.Manager.hasChanged = true;
        };
        Joint.raycastArray = new Array();
        return Joint;
    }());
    exports.Joint = Joint;
});
define("elts/eltInspector", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltInspector = /** @class */ (function () {
        function EltInspector(elt) {
            var _this = this;
            this.htmlContainer = document.createElement("div");
            this.surfaceContainer = document.createElement("div");
            this.htmlContainer.appendChild(this.surfaceContainer);
            this.jointContainer = document.createElement("div");
            this.htmlContainer.appendChild(this.jointContainer);
            var surfaceHeader = document.createElement("p");
            surfaceHeader.textContent = "Surfaces: " + elt.surfaces.length;
            this.surfaceContainer.appendChild(surfaceHeader);
            elt.surfaces.forEach(function (surface) {
                _this.surfaceContainer.appendChild(surface.get_SubInspector().htmlContainer);
            });
            var jointHeader = document.createElement("p");
            surfaceHeader.textContent = "Joints: " + elt.joints.length;
            elt.joints.forEach(function (joint) {
                _this.jointContainer.appendChild(joint.get_SubInspector().htmlContainer);
            });
            var but = document.createElement("button");
            but.textContent = "Delete";
            but.onclick = function () { elt.delete(); };
            this.htmlContainer.appendChild(but);
        }
        return EltInspector;
    }());
    exports.EltInspector = EltInspector;
});
define("elts/elt", ["require", "exports", "elts/eltInspector"], function (require, exports, eltInspector_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Elt = /** @class */ (function () {
        function Elt() {
            //number of elements waiting for in this frame
            this.parentsWaitingFor = 0;
            //elements waiting on this element to finish
            this.children = new Array();
            //elements this element depends on
            this.parents = new Array();
            //surfaces making up this element
            this.surfaces = new Array();
            //joints making up this element
            this.joints = new Array();
        }
        //Keeps track of number of parents ready, called by parent when ready
        Elt.prototype.parentReady = function () {
            if (this.parentsWaitingFor == 0)
                this.parentsWaitingFor = this.parents.length;
            this.parentsWaitingFor--;
            if (this.parentsWaitingFor == 0)
                this.allParentsReady();
        };
        Elt.prototype.allParentsReady = function () {
            //move the surfaces into place
            this.move();
            //might want to add to a list instead of doing nested calling
            //depends on performance and if stacktrace gets overflowed
            //only a problem for huge structures
            this.tellChildrenReady();
        };
        Elt.prototype.tellChildrenReady = function () {
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].parentReady();
            }
        };
        //START: makes sure the call gets forwarded to the other party child/parent*
        //goes parent->child->parent or child->parent->children
        //gets called rarely, so no need to optimize
        Elt.prototype.addChild = function (child) {
            if (this.children.indexOf(child) != -1)
                return;
            this.children.push(child);
            child.addParent(this);
        };
        Elt.prototype.remChild = function (child) {
            var index = this.children.indexOf(child);
            if (index == -1)
                return;
            this.children.splice(index, 1);
            child.remParent(this);
        };
        Elt.prototype.addParent = function (parent) {
            if (this.parents.indexOf(parent) != -1)
                return;
            this.parents.push(parent);
            parent.addChild(this);
        };
        //used when removing a connection or deleteing an Elt
        Elt.prototype.remParent = function (parent) {
            var index = this.parents.indexOf(parent);
            if (index == -1)
                return;
            this.parents.splice(index, 1);
            parent.remChild(this);
        };
        Elt.prototype.get_Inspector = function () {
            if (this.inspector)
                return this.inspector.htmlContainer;
            else
                return null;
        };
        Elt.prototype.createInspector = function () {
            this.inspector = new eltInspector_1.EltInspector(this);
        };
        return Elt;
    }());
    exports.Elt = Elt;
});
define("elts/eltPage", ["require", "exports", "three", "elts/elt", "elts/surface", "misc/manager", "elts/surfaceMesh"], function (require, exports, THREE, elt_1, surface_1, manager_5, surfaceMesh_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltPage = /** @class */ (function (_super) {
        __extends(EltPage, _super);
        function EltPage(color, width, height, thickness, geomTranslateY) {
            var _this = _super.call(this) || this;
            var geometry = new THREE.BoxGeometry(width, thickness, height);
            geometry.translate(width / 2.0, thickness / 2.0 * geomTranslateY, 0);
            var material = new THREE.MeshBasicMaterial({ color: color });
            var mesh = new surfaceMesh_1.SurfaceMesh(geometry, material, "no-drop");
            _this.surfaces.push(new surface_1.Surface(_this, mesh));
            manager_5.Manager.center.add(_this.surfaces[0].container);
            manager_5.Manager.raycastList.push(_this.surfaces[0].mesh);
            return _this;
        }
        EltPage.prototype.move = function () {
        };
        EltPage.prototype.updatePoints = function () {
        };
        EltPage.prototype.delete = function () { };
        return EltPage;
    }(elt_1.Elt));
    exports.EltPage = EltPage;
});
define("misc/skybox", ["require", "exports", "three", "misc/manager"], function (require, exports, THREE, manager_6) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Skybox = /** @class */ (function () {
        function Skybox() {
            this.loadedNum = 0;
            var t = this;
            var materialArray = [];
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_ft.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_bk.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_up.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_dn.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_rt.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            materialArray.push(new THREE.MeshBasicMaterial({ map: new THREE.TextureLoader().load('resources/sor_lake1/lake1_lf.jpg', function () { t.loaded(); }), side: THREE.DoubleSide }));
            for (var i = 0; i < 6; i++)
                materialArray[i].side = THREE.BackSide;
            var skyboxMaterial = materialArray;
            var skyboxGeom = new THREE.CubeGeometry(25, 25, 25, 1, 1, 1);
            this.mesh = new THREE.Mesh(skyboxGeom, skyboxMaterial);
            manager_6.Manager.center.add(this.mesh);
        }
        Skybox.prototype.loaded = function () {
            this.loadedNum++;
            if (this.loadedNum == 6)
                manager_6.Manager.isDirty = true;
        };
        return Skybox;
    }());
    exports.Skybox = Skybox;
});
define("menu/inspector", ["require", "exports", "misc/manager", "elts/jointHandle"], function (require, exports, manager_7, jointHandle_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Inspector = /** @class */ (function () {
        /*htmlHeader:HTMLParagraphElement;
        colorPicker:ColorPicker;
        htmlSurfaceContainer:HTMLDivElement;
        htmlJointContainer:HTMLDivElement;*/
        function Inspector() {
            this.htmlContainer = document.createElement("div");
        }
        Inspector.prototype.select = function (object, event) {
            manager_7.Manager.selectedObject = object;
            var select = manager_7.Manager.selectedObject;
            if (select.selected) {
                select.selected(event);
            }
            if (select instanceof jointHandle_3.JointHandle) {
                if (select.jointOwner)
                    select = select.jointOwner.leaf;
            }
            if (select && select.get_Inspector) {
                this.htmlContainer.innerHTML = "";
                var inspector = select.get_Inspector();
                if (inspector) {
                    this.htmlContainer.appendChild(inspector);
                    return true;
                }
                return true;
            }
            return false;
        };
        Inspector.prototype.selectNothing = function () {
            manager_7.Manager.selectedObject = null;
            this.htmlContainer.innerHTML = "";
        };
        return Inspector;
    }());
    exports.Inspector = Inspector;
});
define("tools/tool", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Tool = /** @class */ (function () {
        function Tool() {
        }
        Tool.prototype.get_ButtonWidget = function () {
            return this.buttonWidget;
        };
        Tool.prototype.get_ToolWidget = function () {
            return this.toolWidget;
        };
        return Tool;
    }());
    exports.Tool = Tool;
});
define("tools/toolDefault", ["require", "exports", "tools/tool", "misc/manager", "elts/joint", "misc/utils", "elts/jointHandle"], function (require, exports, tool_1, manager_8, joint_2, utils_3, jointHandle_4) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ToolDefault = /** @class */ (function (_super) {
        __extends(ToolDefault, _super);
        function ToolDefault() {
            var _this = _super.call(this) || this;
            _this.buttonWidget = document.createElement("p");
            _this.buttonWidget.textContent = "def";
            _this.toolWidget = document.createElement("p");
            _this.toolWidget.textContent = "select/move stuff";
            return _this;
        }
        ToolDefault.prototype.mouseDown = function (event) {
            if (event.buttons % 2 == 1) {
                var arr = new Array();
                arr = manager_8.Manager.raycastList.concat(joint_2.Joint.raycastArray);
                var intersects = utils_3.raycast(arr);
                if (intersects.length > 0 && manager_8.Manager.menuBar.inspector.select(intersects[0].object, event)) {
                    console.log("inspecting");
                    return true;
                }
                else
                    manager_8.Manager.menuBar.inspector.selectNothing();
            }
            return false;
        };
        ToolDefault.prototype.mouseUp = function (event) {
            return false;
        };
        ToolDefault.prototype.mouseMove = function (event) {
            if (event.buttons % 2 == 1) {
                if (manager_8.Manager.selectedObject && manager_8.Manager.selectedObject instanceof jointHandle_4.JointHandle) {
                    manager_8.Manager.selectedObject.mousemove(event);
                    return true;
                }
            }
            return false;
        };
        ToolDefault.prototype.keydown = function (event) {
            //delete
            if (event.keyCode == 46 && manager_8.Manager.selectedObject) {
                if (manager_8.Manager.selectedObject.owner)
                    manager_8.Manager.selectedObject.owner.owner.delete();
                else if (manager_8.Manager.selectedObject.jointOwner)
                    manager_8.Manager.selectedObject.jointOwner.leaf.owner.delete();
                return true;
            }
            return false;
        };
        return ToolDefault;
    }(tool_1.Tool));
    exports.ToolDefault = ToolDefault;
});
define("elts/eltAdd", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltAdd = /** @class */ (function () {
        function EltAdd() {
        }
        return EltAdd;
    }());
    exports.EltAdd = EltAdd;
});
define("elts/eltPageAdd", ["require", "exports", "elts/eltAdd"], function (require, exports, eltAdd_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltPageAdd = /** @class */ (function (_super) {
        __extends(EltPageAdd, _super);
        function EltPageAdd() {
            var _this = _super.call(this) || this;
            _this.icon = document.createElement("p");
            _this.icon.textContent = "Page";
            return _this;
        }
        EltPageAdd.prototype.mouseDown = function (event) { return false; };
        ;
        EltPageAdd.prototype.mouseUp = function (event) { return false; };
        ;
        EltPageAdd.prototype.mouseMove = function (event) { return false; };
        ;
        EltPageAdd.prototype.keydown = function (event) { return false; };
        ;
        return EltPageAdd;
    }(eltAdd_1.EltAdd));
    exports.EltPageAdd = EltPageAdd;
});
define("elts/eltParallel", ["require", "exports", "three", "elts/elt", "elts/surface", "misc/manager", "misc/utils", "elts/surfaceMesh", "elts/eltInspector", "elts/jointHandle"], function (require, exports, THREE, elt_2, surface_2, manager_9, utils_4, surfaceMesh_2, eltInspector_2, jointHandle_5) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltParallel = /** @class */ (function (_super) {
        __extends(EltParallel, _super);
        function EltParallel(jointOne, jointTwo, width, height, color) {
            var _this = _super.call(this) || this;
            //failsafe to prevent division by 0
            //TODO: move to height/width setter
            if (height == 0 || width == 0) {
                console.warn("Warning: width or height is zero for parallel, EltParallel is going to be ignored. Choose another Elt if you want a single surface!");
                return _this;
            }
            _this.joints.push(jointOne);
            _this.joints.push(jointTwo);
            _this.addParent(_this.joints[0].root.owner);
            _this.addParent(_this.joints[1].root.owner);
            _this.height = height;
            _this.width = width;
            var depthOne = jointOne.getDistance();
            var geometryL = new THREE.BoxGeometry(1, 1, 1);
            geometryL.translate(0.5, 0.5, 0.5);
            var depthTwo = jointTwo.getDistance();
            var geometryR = new THREE.BoxGeometry(1, 1, 1);
            geometryR.translate(0.5, 0.5, 0.5);
            var matL = new THREE.MeshBasicMaterial({ color: color });
            var matR = new THREE.MeshBasicMaterial({ color: color + 0x333333 });
            var leftPane = new surfaceMesh_2.SurfaceMesh(geometryL, matL, "grab");
            var rightPane = new surfaceMesh_2.SurfaceMesh(geometryR, matR, "grab");
            _this.surfaces.push(new surface_2.Surface(_this, leftPane));
            _this.joints[0].leaf = _this.surfaces[0];
            _this.surfaces.push(new surface_2.Surface(_this, rightPane));
            _this.joints[1].leaf = _this.surfaces[1];
            manager_9.Manager.raycastList.push(_this.surfaces[0].mesh);
            manager_9.Manager.raycastList.push(_this.surfaces[1].mesh);
            _this.joints[0].pointA.add(_this.surfaces[0].container);
            _this.surfaces[0].mesh.scale.set(width, manager_9.Manager.thickness, depthOne);
            _this.joints[1].pointA.add(_this.surfaces[1].container);
            _this.surfaces[1].mesh.scale.set(height, manager_9.Manager.thickness, depthTwo);
            _this.joints[0].name = "jOne";
            _this.joints[1].name = "jTwo";
            _this.surfaces[0].name = "sOne";
            _this.surfaces[1].name = "sTwo";
            _this.inspector = new eltInspector_2.EltInspector(_this);
            return _this;
        }
        //assumes the two joints are parallel
        EltParallel.prototype.move = function () {
            //START: NEEDED! no clue why :/ (maybe update matrices?...)
            this.joints[0].root.container.getWorldRotation();
            //this.joints[1].surface.mesh.getWorldRotation();
            //END
            //get both joint into global space
            var jointOneAxis = new THREE.Vector3().subVectors(this.joints[0].pointA.getWorldPosition(), this.joints[0].pointB.getWorldPosition()).normalize();
            //get the distance between the two joints by using:
            //https://math.stackexchange.com/questions/1347604/find-3d-distance-between-two-parallel-lines-in-simple-way
            var distanceVec = new THREE.Vector3().crossVectors(jointOneAxis, new THREE.Vector3().subVectors(this.joints[0].pointA.getWorldPosition(), this.joints[1].pointB.getWorldPosition()));
            var distance = distanceVec.length();
            var angleL;
            var angleR;
            //FOR ALL:
            //since parallel, can set Z to 0 and ignore (in the right coordinate system AFTER converting!)
            //everything becomes 2D and is easier
            //get angle between joint<->joint line and finished surface
            //depicted by figure 1-par-angle_dist-top
            //prevent division by 0
            //clamp to -1/1 because of rounding errors and wrong configuration, could break: acos(1.001)=NaN
            if (distance != 0) {
                angleL = Math.acos(utils_4.clamp(((this.width * this.width - distance * distance - this.height * this.height) / (-2 * this.height * distance)), -1, 1));
                angleR = Math.acos(utils_4.clamp(((this.height * this.height - distance * distance - this.width * this.width) / (-2 * distance * this.width)), -1, 1));
            }
            else {
                angleL = Math.PI / 2.0;
                angleR = Math.PI / 2.0;
            }
            //get other joint point into local coordinates of active joint
            var pointTwoA = this.joints[0].root.container.worldToLocal(this.joints[1].pointA.getWorldPosition());
            //get angle between holding surface and joint<->joint line
            //depicted by figure 2-par-angle_dist-surface
            var angleRPSec = Math.atan(-pointTwoA.y / (this.joints[0].pointA.position.x - pointTwoA.x));
            //get other joint point into local coordinates of active joint
            var pointOneA = this.joints[1].root.container.worldToLocal(this.joints[0].pointA.getWorldPosition());
            //get angle between holding surface and joint<->joint line
            var angleLPSec = Math.atan(-pointOneA.y / (this.joints[1].pointA.position.x - pointOneA.x));
            //set angle, make sure doesn't flip over to the other side
            this.surfaces[0].container.rotation.z = (angleR + angleRPSec + Math.PI) % Math.PI + Math.PI;
            this.surfaces[1].container.rotation.z = (-angleL + angleLPSec + Math.PI) % Math.PI;
            this.updateHeights(this.joints[1]);
        };
        EltParallel.prototype.updatePoints = function (joint) {
            if (joint.changed == jointHandle_5.JointHandle.handleType.Center) {
                joint.pointB.position.z = joint.pointA.position.z + joint.cylinder.scale.y;
                joint.pointB.position.x = joint.pointA.position.x;
            }
            else {
                if (joint.changed == jointHandle_5.JointHandle.handleType.A) {
                    joint.pointB.position.x = joint.pointA.position.x;
                }
                else if (joint.changed == jointHandle_5.JointHandle.handleType.B) {
                    joint.pointA.position.x = joint.pointB.position.x;
                }
                var distOne = joint.getDistance();
                joint.cylinder.scale.set(1, distOne, 1);
                joint.leaf.mesh.scale.z = distOne;
            }
            this.updateHeights(joint);
            manager_9.Manager.hasChanged = true;
        };
        EltParallel.prototype.updateHeights = function (joint) {
            //TODO: update surface heights
            var line = this.joints[0].root.intersectSurface(this.joints[1].root);
            var distOne = line.distanceToPoint(this.joints[0].pointA.localToWorld(new THREE.Vector3(0, 0, 0)));
            var distTwo = line.distanceToPoint(this.joints[1].pointA.localToWorld(new THREE.Vector3(0, 0, 0)));
            if (joint == this.joints[0]) {
                this.height = this.width + distOne - distTwo;
            }
            else {
                this.width = this.height + distTwo - distOne;
            }
            var ratio = (distOne + distTwo) / (this.width + this.height);
            if (ratio > 1) {
                this.width *= ratio;
                this.height *= ratio;
            }
            this.joints[0].leaf.mesh.scale.x = this.width;
            this.joints[1].leaf.mesh.scale.x = this.height;
        };
        EltParallel.prototype.delete = function () {
            var _this = this;
            console.log("deleting..");
            this.children.forEach(function (child) {
                child.delete();
            });
            this.joints.forEach(function (joint) {
                joint.delete();
            });
            this.surfaces.forEach(function (surface) {
                surface.delete();
            });
            this.parents.forEach(function (parent) {
                _this.remParent(parent);
            });
            manager_9.Manager.isDirty = true;
        };
        return EltParallel;
    }(elt_2.Elt));
    exports.EltParallel = EltParallel;
});
define("elts/eltBasicAdd", ["require", "exports", "three", "elts/eltAdd", "elts/joint", "misc/utils", "elts/eltParallel", "misc/manager", "elts/surfaceMesh"], function (require, exports, THREE, eltAdd_2, joint_3, utils_5, eltParallel_1, manager_10, surfaceMesh_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltBasicAdd = /** @class */ (function (_super) {
        __extends(EltBasicAdd, _super);
        function EltBasicAdd() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.numOfJointsTotal = 2;
            _this.joints = new Array();
            return _this;
        }
        EltBasicAdd.prototype.mouseDown = function (event) {
            //raycast
            var intersections = utils_5.raycast(manager_10.Manager.raycastList);
            if (intersections.length > 0 && intersections[0].object instanceof surfaceMesh_3.SurfaceMesh) {
                var sel = intersections[0].object;
                var point = intersections[0].point.clone();
                console.log(point);
                point = intersections[0].object.parent.worldToLocal(point);
                console.log(point);
                var p = point;
                var p1 = new THREE.Vector2(p.x, p.z);
                console.log(p1);
                var p2 = p1.clone();
                p2.y += 0.1;
                //sel.owner.leaf.mesh.material.color = 0x111111;
                console.log(sel);
                this.joints.push(new joint_3.Joint(sel.owner, p1, p2));
                if (this.joints.length == this.numOfJointsTotal) {
                    this.addElement();
                }
                return true;
            }
            return false;
        };
        ;
        EltBasicAdd.prototype.addElement = function () {
            console.log("addingElement");
            new eltParallel_1.EltParallel(this.joints[0], this.joints[1], 0.1, 0.1, 0x555555);
            this.joints = new Array();
            manager_10.Manager.isDirty = true;
        };
        EltBasicAdd.prototype.mouseUp = function (event) { return false; };
        ;
        EltBasicAdd.prototype.mouseMove = function (event) { return false; };
        ;
        EltBasicAdd.prototype.keydown = function (event) { return false; };
        ;
        return EltBasicAdd;
    }(eltAdd_2.EltAdd));
    exports.EltBasicAdd = EltBasicAdd;
});
define("elts/eltParallelAdd", ["require", "exports", "elts/eltBasicAdd"], function (require, exports, eltBasicAdd_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltParallelAdd = /** @class */ (function (_super) {
        __extends(EltParallelAdd, _super);
        function EltParallelAdd() {
            var _this = _super.call(this) || this;
            _this.icon = document.createElement("p");
            _this.icon.textContent = "parallel";
            return _this;
        }
        return EltParallelAdd;
    }(eltBasicAdd_1.EltBasicAdd));
    exports.EltParallelAdd = EltParallelAdd;
});
define("elts/eltParallelogramAdd", ["require", "exports", "elts/eltParallelAdd"], function (require, exports, eltParallelAdd_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltParallelogramAdd = /** @class */ (function (_super) {
        __extends(EltParallelogramAdd, _super);
        function EltParallelogramAdd() {
            var _this = _super.call(this) || this;
            _this.icon = document.createElement("p");
            _this.icon.textContent = "parallelogram";
            return _this;
        }
        return EltParallelogramAdd;
    }(eltParallelAdd_1.EltParallelAdd));
    exports.EltParallelogramAdd = EltParallelogramAdd;
});
define("tools/toolAdd", ["require", "exports", "tools/tool", "elts/eltPageAdd", "elts/eltParallelAdd", "elts/eltParallelogramAdd"], function (require, exports, tool_2, eltPageAdd_1, eltParallelAdd_2, eltParallelogramAdd_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ToolAdd = /** @class */ (function (_super) {
        __extends(ToolAdd, _super);
        function ToolAdd() {
            var _this = _super.call(this) || this;
            _this.adders = new Array();
            _this.buttons = new Array();
            _this.selectedAdder = 0;
            _this.buttonWidget = document.createElement("p");
            _this.buttonWidget.textContent = "add";
            _this.toolWidget = document.createElement("div");
            _this.addAdder(new eltPageAdd_1.EltPageAdd());
            _this.addAdder(new eltParallelAdd_2.EltParallelAdd());
            _this.addAdder(new eltParallelogramAdd_1.EltParallelogramAdd());
            return _this;
        }
        ToolAdd.prototype.addAdder = function (elt) {
            var index = this.adders.length;
            this.adders.push(elt);
            var but = document.createElement("button");
            but.appendChild(elt.icon);
            var t = this;
            but.onclick = function () { t.selectedAdder = index; };
            this.buttons.push(but);
            this.toolWidget.appendChild(but);
        };
        ToolAdd.prototype.mouseUp = function (event) { return this.adders[this.selectedAdder].mouseUp(event); };
        ToolAdd.prototype.mouseDown = function (event) { return this.adders[this.selectedAdder].mouseDown(event); };
        ToolAdd.prototype.mouseMove = function (event) { return this.adders[this.selectedAdder].mouseMove(event); };
        ToolAdd.prototype.keydown = function (event) { return this.adders[this.selectedAdder].keydown(event); };
        return ToolAdd;
    }(tool_2.Tool));
    exports.ToolAdd = ToolAdd;
});
define("menu/toolBar", ["require", "exports", "tools/toolDefault", "tools/toolAdd"], function (require, exports, toolDefault_1, toolAdd_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ToolBar = /** @class */ (function () {
        function ToolBar() {
            this.tools = new Array();
            this.toolButtons = new Array();
            this.selectedToolIndex = 0;
            this.htmlContainer = document.createElement("div");
            this.htmlContainer.style.border = "2px";
            this.htmlContainer.style.borderColor = "blue";
            this.htmlContainer.style.borderStyle = "solid";
            this.toolButtonsContainer = document.createElement("div");
            this.htmlContainer.appendChild(this.toolButtonsContainer);
            this.toolContainer = document.createElement("div");
            this.htmlContainer.appendChild(this.toolContainer);
            this.addTool(new toolDefault_1.ToolDefault());
            this.addTool(new toolAdd_1.ToolAdd());
            this.selectTool(0);
        }
        ToolBar.prototype.addTool = function (tool) {
            var but = document.createElement("button");
            var index = this.tools.length;
            this.tools.push(tool);
            var t = this;
            but.onclick = function () { t.selectTool(index); };
            but.appendChild(tool.get_ButtonWidget());
            this.toolButtonsContainer.appendChild(but);
            this.toolButtons.push(but);
        };
        ToolBar.prototype.selectTool = function (index) {
            this.toolButtons[this.selectedToolIndex].style.backgroundColor = "buttonface";
            try {
                this.toolContainer.removeChild(this.tools[this.selectedToolIndex].get_ToolWidget());
            }
            catch (e) { }
            this.selectedTool = this.tools[index];
            this.selectedToolIndex = index;
            this.toolButtons[index].style.backgroundColor = "white";
            this.toolContainer.appendChild(this.tools[this.selectedToolIndex].get_ToolWidget());
        };
        return ToolBar;
    }());
    exports.ToolBar = ToolBar;
});
define("menu/menuBar", ["require", "exports", "menu/inspector", "menu/toolBar"], function (require, exports, inspector_1, toolBar_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var MenuBar = /** @class */ (function () {
        function MenuBar() {
            this.htmlContainer = document.getElementById("menuBar");
            this.htmlContainer.style.maxWidth = "200px";
            this.inspector = new inspector_1.Inspector();
            this.htmlContainer.appendChild(this.inspector.htmlContainer);
            this.toolBar = new toolBar_1.ToolBar();
            this.htmlContainer.appendChild(this.toolBar.htmlContainer);
        }
        return MenuBar;
    }());
    exports.MenuBar = MenuBar;
});
define("misc/manager", ["require", "exports", "three", "misc/camera", "elts/eltPage", "misc/skybox", "menu/menuBar"], function (require, exports, THREE, camera_1, eltPage_1, skybox_1, menuBar_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Manager = /** @class */ (function () {
        function Manager() {
        }
        Manager.build = function () {
            Manager.scene = new THREE.Scene();
            Manager.camera = new camera_1.Camera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
            Manager.camera.position.z = -3;
            Manager.renderer = new THREE.WebGLRenderer({ antialias: true });
            Manager.renderer.setSize(window.innerWidth - 200, window.innerHeight - 0);
            document.body.appendChild(Manager.renderer.domElement);
            Manager.center = new THREE.Object3D();
            Manager.scene.add(Manager.center);
            Manager.raycastList = new Array();
            Manager.selectedObject = null;
            Manager.mouse = new THREE.Vector2(-5, -5);
            Manager.pageThickness = 0.05;
            Manager.pageLeft = new eltPage_1.EltPage(0x888877, 1, 1, Manager.pageThickness, 1);
            Manager.pageRight = new eltPage_1.EltPage(0x555544, 1, 1, Manager.pageThickness, -1);
            Manager.thickness = 0.01;
            Manager.toMove = 0;
            Manager.raycaster = new THREE.Raycaster();
            Manager.skybox = new skybox_1.Skybox();
            Manager.camera.setCameraPos(0, 0);
            Manager.menuBar = new menuBar_1.MenuBar();
        };
        Manager.isDirty = true;
        Manager.hasChanged = true;
        return Manager;
    }());
    exports.Manager = Manager;
});
define("elts/eltParallelogram", ["require", "exports", "three", "elts/eltParallel"], function (require, exports, THREE, eltParallel_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltParallelogram = /** @class */ (function (_super) {
        __extends(EltParallelogram, _super);
        function EltParallelogram(jointOne, jointTwo, color) {
            return _super.call(this, jointOne, jointTwo, jointTwo.pointOne.x - 0, jointOne.pointOne.x - 0, color) || this;
        }
        EltParallelogram.prototype.updateHeights = function (joint) {
            var line = this.joints[0].root.intersectSurface(this.joints[1].root);
            var distOne = line.distanceToPoint(this.joints[0].pointA.localToWorld(new THREE.Vector3(0, 0, 0)));
            var distTwo = line.distanceToPoint(this.joints[1].pointA.localToWorld(new THREE.Vector3(0, 0, 0)));
            this.height = distOne;
            this.width = distTwo;
            this.joints[0].leaf.mesh.scale.x = this.width;
            this.joints[1].leaf.mesh.scale.x = this.height;
        };
        return EltParallelogram;
    }(eltParallel_2.EltParallel));
    exports.EltParallelogram = EltParallelogram;
});
define("elts/eltVFold", ["require", "exports", "three", "elts/elt", "misc/manager", "elts/surface", "misc/utils", "elts/surfaceMesh"], function (require, exports, THREE, elt_3, manager_11, surface_3, utils_6, surfaceMesh_4) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltVFold = /** @class */ (function (_super) {
        __extends(EltVFold, _super);
        function EltVFold(jointOne, hOne, heightOne, jointTwo, hTwo, heightTwo, depth) {
            var _this = _super.call(this) || this;
            _this.joints.push(jointOne);
            _this.heightOne = heightOne;
            _this.joints.push(jointTwo);
            _this.heightTwo = heightTwo;
            _this.depthOne = jointOne.getDistance();
            _this.depthTwo = jointTwo.getDistance();
            _this.addParent(_this.joints[0].root.owner);
            _this.addParent(_this.joints[1].root.owner);
            var geometryL = _this.createGeom(jointOne.pointOne, _this.depthOne, hOne, heightOne, manager_11.Manager.thickness);
            var geometryR = _this.createGeom(jointTwo.pointOne, _this.depthTwo, hTwo, heightTwo, manager_11.Manager.thickness);
            var matL = new THREE.MeshBasicMaterial({ color: 0x8888aa });
            var matR = new THREE.MeshBasicMaterial({ color: 0x88aa77 });
            var leftPane = new surfaceMesh_4.SurfaceMesh(geometryL, matL, "grab");
            _this.surfaces.push(new surface_3.Surface(_this, leftPane));
            manager_11.Manager.raycastList.push(_this.surfaces[0].mesh);
            _this.joints[0].leaf = _this.surfaces[0];
            var rightPane = new surfaceMesh_4.SurfaceMesh(geometryR, matR, "grab");
            _this.surfaces.push(new surface_3.Surface(_this, rightPane));
            _this.joints[1].leaf = _this.surfaces[1];
            manager_11.Manager.raycastList.push(_this.surfaces[1].mesh);
            _this.joints[0].pointA.add(_this.surfaces[0].container);
            _this.joints[1].pointA.add(_this.surfaces[1].container);
            //division by 0 is cool: infinity which is handled nicely by atan(infinity)=Math.PI
            //this.joints[0].pointOne.x = 0.001;
            _this.joints[0].pointA.rotation.y = -Math.atan((_this.joints[0].pointTwo.y - _this.joints[0].pointOne.y) / (_this.joints[0].pointTwo.x - _this.joints[0].pointOne.x)) + Math.PI / 2;
            _this.joints[1].pointA.rotation.y = -Math.atan((_this.joints[1].pointTwo.y - _this.joints[1].pointOne.y) / (_this.joints[1].pointTwo.x - _this.joints[1].pointOne.x)) + Math.PI / 2;
            _this.updatePoints();
            return _this;
        }
        EltVFold.prototype.move = function () {
            var guliAngle = this.joints[0].root.container.getWorldRotation().z - this.joints[1].root.container.getWorldRotation().z;
            var c = this.joints[0].pointOne.x;
            var a = this.joints[1].pointOne.x;
            var diag = Math.sqrt(c * c + a * a - 2 * a * c * Math.cos(guliAngle));
            var width = this.heightTwo;
            var height = this.heightOne;
            //clamp necessary: rounding error > 1 or < -1
            var angleOne = Math.acos(utils_6.clamp((a * a - c * c - diag * diag) / (-2 * c * diag), -1, 1)) + Math.acos(utils_6.clamp((height * height - width * width - diag * diag) / (-2 * width * diag), -1, 1));
            var angleTwo = Math.acos(utils_6.clamp((c * c - a * a - diag * diag) / (-2 * a * diag), -1, 1)) + Math.acos(utils_6.clamp((width * width - height * height - diag * diag) / (-2 * height * diag), -1, 1));
            this.surfaces[0].container.rotation.z = angleOne + Math.PI;
            this.surfaces[1].container.rotation.z = angleTwo;
            var S = utils_6.get_S(this.joints[0].pointA.getWorldPosition(), this.joints[0].pointB.getWorldPosition(), this.joints[1].pointA.getWorldPosition(), this.joints[1].pointB.getWorldPosition(), this.heightOne);
        };
        EltVFold.prototype.createGeom = function (point, depth, h, height, thickness) {
            var geometry = new THREE.Geometry();
            geometry.vertices.push(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, 1), new THREE.Vector3(height, 0, 1), new THREE.Vector3(h, 0, 0));
            geometry.faces.push(new THREE.Face3(0, 1, 2), new THREE.Face3(0, 2, 1), new THREE.Face3(0, 2, 3), new THREE.Face3(0, 3, 2));
            geometry.computeBoundingSphere();
            return geometry;
        };
        ;
        EltVFold.prototype.updatePoints = function () {
            var distOne = this.joints[0].getDistance();
            this.joints[0].cylinder.scale.set(1, distOne, 1);
            this.surfaces[0].mesh.scale.z = distOne;
            var distTwo = this.joints[1].getDistance();
            this.joints[1].cylinder.scale.set(1, distTwo, 1);
            this.surfaces[1].mesh.scale.z = distTwo;
        };
        ;
        EltVFold.prototype.delete = function () {
            console.log("deleting");
        };
        return EltVFold;
    }(elt_3.Elt));
    exports.EltVFold = EltVFold;
});
define("main", ["require", "exports", "three", "misc/manager", "elts/jointHandle", "misc/utils", "elts/joint", "elts/eltParallel", "elts/eltParallelogram"], function (require, exports, THREE, manager_12, jointHandle_6, utils_7, joint_4, eltParallel_3, eltParallelogram_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    manager_12.Manager.build();
    function wheelRotation(e) {
        if (e.deltaY > 0)
            manager_12.Manager.toMove += Math.PI / 20.0;
        else if (e.deltaY < 0)
            manager_12.Manager.toMove -= Math.PI / 20.0;
    }
    manager_12.Manager.renderer.domElement.addEventListener("wheel", wheelRotation, { passive: true });
    window.oncontextmenu = function (event) {
        //event.preventDefault();
        return false;
    };
    //TODO: prevent del from triggering when inside input
    window.onkeydown = function (event) {
        if (manager_12.Manager.menuBar.toolBar.selectedTool.keydown(event)) {
            event.preventDefault();
        }
    };
    manager_12.Manager.renderer.domElement.ondblclick = function (event) {
        event.preventDefault();
        console.log("prevented");
    };
    manager_12.Manager.renderer.domElement.onmousedown = function (event) {
        manager_12.Manager.menuBar.toolBar.selectedTool.mouseDown(event);
    };
    manager_12.Manager.renderer.domElement.onmouseup = function (event) {
        manager_12.Manager.menuBar.toolBar.selectedTool.mouseUp(event);
    };
    manager_12.Manager.renderer.domElement.onmousemove = function (event) {
        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
        manager_12.Manager.mouse.x = (event.offsetX / manager_12.Manager.renderer.domElement.width) * 2 - 1;
        manager_12.Manager.mouse.y = -(event.offsetY / manager_12.Manager.renderer.domElement.height) * 2 + 1;
        if (!manager_12.Manager.menuBar.toolBar.selectedTool.mouseMove(event)) {
            if (event.buttons % 2 == 1) {
                manager_12.Manager.camera.move(event);
                manager_12.Manager.isDirty = true;
            }
        }
    };
    window.onresize = function (event) {
        manager_12.Manager.camera.aspect = window.innerWidth / window.innerHeight;
        manager_12.Manager.camera.updateProjectionMatrix();
        manager_12.Manager.renderer.setSize(window.innerWidth - 200, window.innerHeight - 0);
        manager_12.Manager.isDirty = true;
    };
    function animate() {
        requestAnimationFrame(animate);
        cannonUpdate();
        var rot = manager_12.Manager.pageLeft.surfaces[0].container.rotation.z;
        if (manager_12.Manager.toMove > 0.1) {
            rot += 0.1;
            manager_12.Manager.toMove -= 0.1;
        }
        else if (manager_12.Manager.toMove < -0.1) {
            rot -= 0.1;
            manager_12.Manager.toMove += 0.1;
        }
        else
            manager_12.Manager.toMove = 0;
        rot = utils_7.clamp(rot, 0.001, Math.PI - 0.001);
        if (manager_12.Manager.pageLeft.surfaces[0].container.rotation.z != rot) {
            manager_12.Manager.pageLeft.surfaces[0].container.rotation.z = rot;
            manager_12.Manager.hasChanged = true;
        }
        if (manager_12.Manager.hasChanged) {
            manager_12.Manager.pageLeft.tellChildrenReady();
            manager_12.Manager.pageRight.tellChildrenReady();
            manager_12.Manager.isDirty = true;
        }
        var intersects = utils_7.raycast(joint_4.Joint.raycastArray);
        if (intersects.length > 0) {
            if (intersects[0].object instanceof jointHandle_6.JointHandle) {
                document.body.style.cursor = intersects[0].object.cursor;
            }
            else
                document.body.style.cursor = "default";
        }
        else {
            document.body.style.cursor = "default";
        }
        /*for ( var i = 0; i < intersects.length && i < 1; i++ ) {
    
            //intersects[ i ].object.material.color.set( 0xff0000 );
            var pos = center.worldToLocal(intersects[i].point);
            sphere.position.set(pos.x,pos.y,pos.z);
        }*/
        if (manager_12.Manager.isDirty)
            manager_12.Manager.renderer.render(manager_12.Manager.scene, manager_12.Manager.camera);
        manager_12.Manager.isDirty = false;
        manager_12.Manager.hasChanged = false;
    }
    var par = new eltParallel_3.EltParallel(new joint_4.Joint(manager_12.Manager.pageLeft.surfaces[0], new THREE.Vector2(0.3, 0), new THREE.Vector2(0.3, 0.1)), new joint_4.Joint(manager_12.Manager.pageRight.surfaces[0], new THREE.Vector2(0.2, 0), new THREE.Vector2(0.2, 0.2)), 0.5, 0.6, 0x882222);
    /*var par2 = new EltParallel(new Joint(par.surfaces[1],new THREE.Vector2(0.5,0.1),new THREE.Vector2(0.5,0.2)),new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.4,0), new THREE.Vector2(0.4,0.2)),0.2,0.5,0x222288);
    var par3 = new EltParallel(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.5,0),new THREE.Vector2(0.5,0.1)),new Joint(par.surfaces[0],new THREE.Vector2(0.5,0), new THREE.Vector2(0.5,0.2)),0.5,0.2,0x228822);
    var par4 = new EltParallel(new Joint(par.surfaces[1],new THREE.Vector2(0.6,0.1),new THREE.Vector2(0.6,0.2)),new Joint(par2.surfaces[0],new THREE.Vector2(0.1,0), new THREE.Vector2(0.1,0.2)),0.1,0.1,0x888822);
    */ var para = new eltParallelogram_1.EltParallelogram(new joint_4.Joint(manager_12.Manager.pageLeft.surfaces[0], new THREE.Vector2(0.5, 0.5), new THREE.Vector2(0.5, 0.7)), new joint_4.Joint(manager_12.Manager.pageRight.surfaces[0], new THREE.Vector2(0.2, 0.5), new THREE.Vector2(0.2, 0.7)), 0x228888);
    /*//var para2 = new EltParallelogram(new AttachmentPoint(pageLeft,new THREE.Vector2(0.5,0.5)),new AttachmentPoint(pageRight,new THREE.Vector2(0.2,0.5)),0.5);
    var vfold = new EltVFold(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.1,0.1),new THREE.Vector2(0.5,0.5)),0.1,0.72,new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.1,0.1),new THREE.Vector2(0.5,0.5)),0.1,0.8,0.3);
    */
    var world = new CANNON.World();
    var geometry = new THREE.BoxGeometry(0.2, 0.00001, 0.2);
    var timeStep = 1 / 60.0;
    var number = 20;
    var meshes = [];
    var bodies = new Array();
    var springs = [];
    var hinges = new Array();
    var p2ps = new Array();
    function cannonTest() {
        world.gravity.set(0, -9.82 * 0.01, 0);
        world.broadphase = new CANNON.NaiveBroadphase();
        world.solver.iterations = 1;
        var shape = new CANNON.Box(new CANNON.Vec3(0.2, 0.00001, 0.2));
        var mass = 1;
        for (var i = 0; i < number; i++) {
            var material = new THREE.MeshBasicMaterial({ color: i * 150, wireframe: true });
            var mesh = new THREE.Mesh(geometry, material);
            manager_12.Manager.center.add(mesh);
            meshes.push(mesh);
        }
        for (var i = 0; i < number; i++) {
            var body = new CANNON.Body({
                mass: i != 0 && i < (number - 1) ? 1 : 0
            });
            body.addShape(shape);
            body.angularDamping = 0.5;
            body.position.set((i - number / 2.0) * 0.2, 0, 0);
            body.linearDamping = 0.5;
            body.collisionFilterGroup = 2;
            body.collisionFilterMask = 2;
            world.addBody(body);
            bodies.push(body);
        }
        //bodies[bodies.length-2].mass = 0;
        //bodies[bodies.length-1].position.set(1,0,0);
        //bodies[bodies.length-2].sleep();
        console.log(bodies[bodies.length - 1].mass);
        /*for(var i = 1; i < number; i++)
        {
            var spring = new CANNON.Spring({
                restLength : 0.1,
                stiffness : 1000,
                damping : 10000,
                localAnchorA: new CANNON.Vec3(-0.1,0,0),
                localAnchorB: new CANNON.Vec3(0.1,0,0),
            });
            spring.bodyA = bodies[i-1];
            spring.bodyB = bodies[i];
            springs.push(spring);
        }*/
        /*for(var i = 1; i < number; i++)
        {
            var hinge = new CANNON.HingeConstraint(bodies[i-1],bodies[i],{
                pivotA: new CANNON.Vec3(0.1,0,0),
                axisA: new CANNON.Vec3(0,0,1),
                pivotB: new CANNON.Vec3(-0.1,0,0),
                axisB: new CANNON.Vec3(0,0,1),
                maxForce: i != 0 && i !=(bodies.length-1) ? 100000000:1,
            });
            hinges.push(hinge);
            world.addConstraint(hinge);
        }*/
        bodies[0].collisionFilterGroup = 1;
        bodies[bodies.length - 1].collisionFilterGroup = 1;
        for (var i = 1; i < number; i++) {
            var p2p1 = new CANNON.PointToPointConstraint(bodies[i - 1], new CANNON.Vec3(0.1, 0, -0.1), bodies[i], new CANNON.Vec3(-0.1, 0, -0.1), 100);
            var p2p2 = new CANNON.PointToPointConstraint(bodies[i - 1], new CANNON.Vec3(0.1, 0, 0.1), bodies[i], new CANNON.Vec3(-0.1, 0, 0.1), 100);
            var dist = p2ps.push(p2p1);
            p2ps.push(p2p2);
            world.addConstraint(p2p1);
            world.addConstraint(p2p2);
        }
        // Compute the force after each step
        world.addEventListener("preStep", function (event) {
            springs.forEach(function (spring) {
                spring.applyForce();
            });
        });
    }
    cannonTest();
    var cannonCounter = 600;
    function cannonUpdate() {
        if (manager_12.Manager.hasChanged) {
            cannonCounter = 600;
        }
        if (cannonCounter > 0) {
            cannonCounter--;
            for (var i = 0; i < 60; i++) {
                world.step(timeStep);
            }
        }
        // Copy coordinates from Cannon.js to Three.js
        for (var i = 0; i < number; i++) {
            meshes[i].position.copy(bodies[i].position);
            meshes[i].quaternion.copy(bodies[i].quaternion);
        }
        manager_12.Manager.isDirty = true;
    }
    window.onkeydown = function (event) {
        var mov = 0;
        if (event.key == "a") {
            mov = 0.05;
        }
        if (event.key == "z") {
            mov = -0.05;
        }
        if (mov != 0) {
            for (var i = 0; i < 1; i++) {
                bodies[0].position.x += mov / 1.0;
                bodies[bodies.length - 1].position.x -= mov / 1.0;
                world.step(timeStep);
            }
            manager_12.Manager.hasChanged = true;
        }
    };
    animate();
});
define("elts/eltElastica", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EltElastica = /** @class */ (function () {
        function EltElastica() {
            var world = new CANNON.World();
            world.gravity.set(0, 0, -9.82);
        }
        return EltElastica;
    }());
    exports.EltElastica = EltElastica;
});
//# sourceMappingURL=bundle.js.map