import {Surface} from "./surface"
import {Joint} from "./joint"
import {EltInspector} from "./eltInspector"
import {Iinspectable} from "menu/iinspectable"

export abstract class Elt implements Iinspectable{
    //number of elements waiting for in this frame
    parentsWaitingFor:number = 0;
    //elements waiting on this element to finish
    children:Array<Elt> = new Array<Elt>();
    //elements this element depends on
    parents:Array<Elt> = new Array<Elt>();
    //surfaces making up this element
    surfaces:Array<Surface> = new Array<Surface>();
    //joints making up this element
    joints:Array<Joint> = new Array<Joint>();

    inspector:EltInspector;

    //Keeps track of number of parents ready, called by parent when ready
    parentReady() {
        if(this.parentsWaitingFor == 0)
            this.parentsWaitingFor = this.parents.length;
        this.parentsWaitingFor--;
        if(this.parentsWaitingFor==0)
            this.allParentsReady();
    }
    allParentsReady() {
        //move the surfaces into place
        this.move();
    
        //might want to add to a list instead of doing nested calling
        //depends on performance and if stacktrace gets overflowed
        //only a problem for huge structures
        this.tellChildrenReady();
    }
    //move the surfaces into place
    abstract move();
    abstract updatePoints(joint:Joint);
    tellChildrenReady() {
        for ( var i = 0; i < this.children.length; i++ ) {
            this.children[i].parentReady();
        }
    }
    //START: makes sure the call gets forwarded to the other party child/parent*
    //goes parent->child->parent or child->parent->children
    //gets called rarely, so no need to optimize
    addChild(child){
        if ( this.children.indexOf(child) != -1 ) return;
        this.children.push(child);
        child.addParent(this);
    }
    remChild(child) {
        var index = this.children.indexOf(child);
        if(index == -1) return;
        this.children.splice(index,1);
        child.remParent(this);
    }
    addParent(parent){
        if ( this.parents.indexOf(parent) != -1 ) return;
        this.parents.push(parent);
        parent.addChild(this);
    }
    //used when removing a connection or deleteing an Elt
    remParent(parent) {
        var index = this.parents.indexOf(parent);
        if(index == -1) return;
        this.parents.splice(index,1);
        parent.remChild(this);
    }
    abstract delete();
    get_Inspector():HTMLElement{
        if(this.inspector)
            return this.inspector.htmlContainer;
        else return null;
    }
    createInspector(){
        this.inspector = new EltInspector(this);
    }
}