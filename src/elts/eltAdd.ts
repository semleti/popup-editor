
export abstract class EltAdd{
    icon: HTMLElement;
    abstract mouseDown(event:MouseEvent):boolean;
    abstract mouseUp(event:MouseEvent):boolean;
    abstract mouseMove(event:MouseEvent):boolean;
    abstract keydown(event:KeyboardEvent):boolean;
}