import * as THREE from "three"
import {EltAdd} from "./eltAdd"
import {Joint} from "./joint"
import {raycast} from "misc/utils"
import {Surface} from "./surface"
import {EltParallel} from "./eltParallel"
import {Manager} from "misc/manager"
import {SurfaceMesh} from "./surfaceMesh"

export abstract class EltBasicAdd extends EltAdd{
    numOfJointsTotal:number = 2;
    joints:Array<Joint> = new Array<Joint>();
    mouseDown(event:MouseEvent):boolean{
        //raycast
        var intersections = raycast(Manager.raycastList);
        if(intersections.length > 0 && intersections[0].object instanceof SurfaceMesh)
        {
            var sel = <SurfaceMesh>intersections[0].object;
            var point = intersections[0].point.clone();
            console.log(point);
            point = (<SurfaceMesh>intersections[0].object).parent.worldToLocal(point);
            console.log(point);
            var p = point;
            var p1 = new THREE.Vector2(p.x,p.z);
            console.log(p1);
            var p2 = p1.clone();
            p2.y += 0.1;
            //sel.owner.leaf.mesh.material.color = 0x111111;
            console.log(sel);
            this.joints.push(new Joint(sel.owner,p1,p2));
            if(this.joints.length == this.numOfJointsTotal)
            {
                this.addElement();
            }
            return true;
        }
        return false;
    };
    addElement(){
        console.log("addingElement");
        new EltParallel(this.joints[0],this.joints[1],0.1,0.1,0x555555);
        this.joints = new Array<Joint>();
        Manager.isDirty = true;
    }
    mouseUp(event:MouseEvent):boolean{return false;};
    mouseMove(event:MouseEvent):boolean{return false;};
    keydown(event:KeyboardEvent):boolean{return false;};
}