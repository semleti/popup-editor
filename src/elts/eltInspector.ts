import {ColorPicker} from "menu/colorPicker"
import {Manager} from "misc/manager"
import {Elt} from "./elt"

export class EltInspector{
    htmlContainer:HTMLElement;
    surfaceContainer:HTMLDivElement;
    jointContainer:HTMLDivElement;
    constructor(elt:Elt){
        this.htmlContainer = document.createElement("div");
        this.surfaceContainer = document.createElement("div");
        this.htmlContainer.appendChild(this.surfaceContainer);
        this.jointContainer = document.createElement("div");
        this.htmlContainer.appendChild(this.jointContainer);
        var surfaceHeader = document.createElement("p");
        surfaceHeader.textContent = "Surfaces: " + elt.surfaces.length;
        this.surfaceContainer.appendChild(surfaceHeader);
        elt.surfaces.forEach(surface => {
            this.surfaceContainer.appendChild(surface.get_SubInspector().htmlContainer);
        });
        var jointHeader = document.createElement("p");
        surfaceHeader.textContent = "Joints: " + elt.joints.length;
        elt.joints.forEach(joint => {
            this.jointContainer.appendChild(joint.get_SubInspector().htmlContainer);
        });
        var but = document.createElement("button");
        but.textContent = "Delete";
        but.onclick = function(){elt.delete();};
        this.htmlContainer.appendChild(but);
    }
}