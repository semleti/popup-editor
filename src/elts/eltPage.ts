import * as THREE from "three"
import {Elt} from "./elt"
import {Surface} from "./surface"
import {Manager} from "../misc/manager"
import {SurfaceMesh} from "elts/surfaceMesh"
export class EltPage extends Elt{
    constructor(color:number, width:number, height:number, thickness:number,geomTranslateY:number){
        super();
        var geometry = new THREE.BoxGeometry( width, thickness, height );
        geometry.translate(width / 2.0,thickness / 2.0*geomTranslateY ,0);
        var material = new THREE.MeshBasicMaterial( { color: color } );
        var mesh = new SurfaceMesh( geometry, material,"no-drop" );
        this.surfaces.push(new Surface(this,mesh));
        Manager.center.add( this.surfaces[0].container );
        Manager.raycastList.push(this.surfaces[0].mesh);
    }
    move(){

    }
    updatePoints(){
        
    }
    delete(){}
}