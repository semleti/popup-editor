import {EltAdd} from "./eltAdd"

export class EltPageAdd extends EltAdd{
    constructor(){
        super();
        this.icon = document.createElement("p");
        this.icon.textContent = "Page";
    }
    mouseDown(event:MouseEvent):boolean{return false;};
    mouseUp(event:MouseEvent):boolean{return false;};
    mouseMove(event:MouseEvent):boolean{return false;};
    keydown(event:KeyboardEvent):boolean{return false;};
}