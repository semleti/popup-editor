import * as THREE from "three"
import {Elt} from "./elt"
import {Joint} from "./joint"
import {Surface} from "./surface"
import {Manager} from "misc/manager"
import {clamp} from "misc/utils"
import {SurfaceMesh}  from "elts/surfaceMesh"
import {EltInspector} from "./eltInspector"
import {JointHandle} from "./jointHandle"
export class EltParallel extends Elt{
    height:number;
    width:number;

    constructor(jointOne:Joint, jointTwo:Joint,width:number,height:number,color:number) {
        super();
        //failsafe to prevent division by 0
        //TODO: move to height/width setter
        if(height == 0 || width == 0){console.warn("Warning: width or height is zero for parallel, EltParallel is going to be ignored. Choose another Elt if you want a single surface!");return;}
        this.joints.push(jointOne);
        this.joints.push(jointTwo);
        this.addParent(this.joints[0].root.owner);
        this.addParent(this.joints[1].root.owner);
        this.height = height;
        this.width = width;
        var depthOne = jointOne.getDistance();
        var geometryL = new THREE.BoxGeometry( 1,1,1 );
        geometryL.translate(0.5,0.5,0.5);
        var depthTwo = jointTwo.getDistance();
        var geometryR = new THREE.BoxGeometry( 1,1,1 );
        geometryR.translate(0.5,0.5,0.5);
        var matL = new THREE.MeshBasicMaterial( { color: color } );
        var matR = new THREE.MeshBasicMaterial( { color: color + 0x333333} );
        var leftPane = new SurfaceMesh( geometryL, matL, "grab");
        var rightPane = new SurfaceMesh( geometryR, matR, "grab");
        this.surfaces.push(new Surface(this,leftPane));
        this.joints[0].leaf = this.surfaces[0];
        this.surfaces.push(new Surface(this,rightPane));
        this.joints[1].leaf = this.surfaces[1];
        Manager.raycastList.push(this.surfaces[0].mesh);
        Manager.raycastList.push(this.surfaces[1].mesh);
        this.joints[0].pointA.add(this.surfaces[0].container);
        this.surfaces[0].mesh.scale.set(width, Manager.thickness, depthOne);
        this.joints[1].pointA.add(this.surfaces[1].container);
        this.surfaces[1].mesh.scale.set(height, Manager.thickness, depthTwo);

        (<any>this.joints[0]).name="jOne";
        (<any>this.joints[1]).name="jTwo";
        (<any>this.surfaces[0]).name="sOne";
        (<any>this.surfaces[1]).name="sTwo";

        this.inspector = new EltInspector(this);
    }
    //assumes the two joints are parallel
    move() {
        //START: NEEDED! no clue why :/ (maybe update matrices?...)
        this.joints[0].root.container.getWorldRotation();
        //this.joints[1].surface.mesh.getWorldRotation();
        //END
    
        //get both joint into global space
        var jointOneAxis = new THREE.Vector3().subVectors(this.joints[0].pointA.getWorldPosition(),this.joints[0].pointB.getWorldPosition()).normalize();
        //get the distance between the two joints by using:
        //https://math.stackexchange.com/questions/1347604/find-3d-distance-between-two-parallel-lines-in-simple-way
        var distanceVec = new THREE.Vector3().crossVectors(jointOneAxis,new THREE.Vector3().subVectors(this.joints[0].pointA.getWorldPosition(),this.joints[1].pointB.getWorldPosition()));
        var distance = distanceVec.length();
        var angleL;
        var angleR;
    
        //FOR ALL:
        //since parallel, can set Z to 0 and ignore (in the right coordinate system AFTER converting!)
        //everything becomes 2D and is easier
    
        //get angle between joint<->joint line and finished surface
        //depicted by figure 1-par-angle_dist-top
        //prevent division by 0
        //clamp to -1/1 because of rounding errors and wrong configuration, could break: acos(1.001)=NaN
        if(distance != 0)
        {
            angleL = Math.acos(clamp(((this.width*this.width - distance*distance - this.height*this.height)/(-2*this.height*distance)),-1,1));
            angleR = Math.acos(clamp(((this.height*this.height - distance*distance - this.width*this.width)/(-2*distance*this.width)),-1,1));
        }
        //division by 0 so set value manually
        //shouldn't happen (rounding errors), but you never know
        else
        {
            angleL = Math.PI/2.0;
            angleR = Math.PI/2.0;
        }
    
    
        //get other joint point into local coordinates of active joint
        var pointTwoA = this.joints[0].root.container.worldToLocal(this.joints[1].pointA.getWorldPosition());
        //get angle between holding surface and joint<->joint line
        //depicted by figure 2-par-angle_dist-surface
        var angleRPSec = Math.atan(-pointTwoA.y / (this.joints[0].pointA.position.x-pointTwoA.x));
    
        //get other joint point into local coordinates of active joint
        var pointOneA = this.joints[1].root.container.worldToLocal(this.joints[0].pointA.getWorldPosition());
        //get angle between holding surface and joint<->joint line
        var angleLPSec = Math.atan(-pointOneA.y / (this.joints[1].pointA.position.x-pointOneA.x));
    
        //set angle, make sure doesn't flip over to the other side
        this.surfaces[0].container.rotation.z = (angleR + angleRPSec + Math.PI)%Math.PI+Math.PI;
        this.surfaces[1].container.rotation.z = (-angleL + angleLPSec + Math.PI)%Math.PI;
        this.updateHeights(this.joints[1]);
    }
    updatePoints(joint:Joint){
        if(joint.changed == JointHandle.handleType.Center)
        {
            joint.pointB.position.z = joint.pointA.position.z + joint.cylinder.scale.y;
            joint.pointB.position.x = joint.pointA.position.x;
        }
        else
        {
            if(joint.changed == JointHandle.handleType.A)
            {
                joint.pointB.position.x = joint.pointA.position.x;
            }
            else if(joint.changed == JointHandle.handleType.B)
            {
                joint.pointA.position.x = joint.pointB.position.x;
            }
            var distOne = joint.getDistance();
            joint.cylinder.scale.set(1,distOne,1);
            joint.leaf.mesh.scale.z = distOne;
        }
        
        this.updateHeights(joint);
        Manager.hasChanged = true;
    }
    updateHeights(joint:Joint){
        //TODO: update surface heights
        var line = this.joints[0].root.intersectSurface(this.joints[1].root);
        var distOne = line.distanceToPoint(this.joints[0].pointA.localToWorld(new THREE.Vector3(0,0,0)));
        var distTwo = line.distanceToPoint(this.joints[1].pointA.localToWorld(new THREE.Vector3(0,0,0)));
        if(joint == this.joints[0])
        {
            this.height = this.width+distOne-distTwo;
        }
        else
        {
            this.width = this.height+distTwo-distOne;
        }
        
        var ratio = (distOne+distTwo)/(this.width+this.height);
        if( ratio > 1)
        {
            this.width *= ratio;
            this.height *= ratio;
        }
        this.joints[0].leaf.mesh.scale.x = this.width;
        this.joints[1].leaf.mesh.scale.x = this.height;
    }
    delete(){
        console.log("deleting..");
        this.children.forEach(child => {
            child.delete();
        });
        this.joints.forEach(joint => {
            joint.delete();
        });
        this.surfaces.forEach(surface => {
            surface.delete();
        });
        this.parents.forEach(parent => {
            this.remParent(parent);
        });
        Manager.isDirty = true;
    }
}