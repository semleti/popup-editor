import {EltBasicAdd} from "./eltBasicAdd"

export class EltParallelAdd extends EltBasicAdd{
    constructor(){
        super();
        this.icon = document.createElement("p");
        this.icon.textContent = "parallel";
    }
}