import * as THREE from "three"
import {EltParallel} from "./eltParallel"
import {Joint} from "./joint"
export class EltParallelogram extends EltParallel{
    constructor(jointOne:Joint, jointTwo:Joint,color:number)
    {
        super(jointOne,jointTwo,jointTwo.pointOne.x-0,jointOne.pointOne.x-0,color);
    }
    updateHeights(joint:Joint){
        var line = this.joints[0].root.intersectSurface(this.joints[1].root);
        var distOne = line.distanceToPoint(this.joints[0].pointA.localToWorld(new THREE.Vector3(0,0,0)));
        var distTwo = line.distanceToPoint(this.joints[1].pointA.localToWorld(new THREE.Vector3(0,0,0)));
        this.height = distOne;
        this.width = distTwo;
        this.joints[0].leaf.mesh.scale.x = this.width;
        this.joints[1].leaf.mesh.scale.x = this.height;
    }
}