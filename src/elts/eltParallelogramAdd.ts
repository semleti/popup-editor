import {EltParallelAdd} from "./eltParallelAdd"

export class EltParallelogramAdd extends EltParallelAdd{
    constructor(){
        super();
        this.icon = document.createElement("p");
        this.icon.textContent = "parallelogram";
    }
    
}