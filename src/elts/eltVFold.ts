import * as THREE from "three"
import {Elt} from "./elt"
import {Joint} from "./joint"
import {Manager} from "misc/manager"
import {Surface} from "./surface"
import {clamp,get_S} from "misc/utils"
import {SurfaceMesh} from "./surfaceMesh"
export class EltVFold extends Elt{
    heightOne:number;
    depthOne:number;
    heightTwo:number;
    depthTwo:number;
    constructor(jointOne, hOne, heightOne, jointTwo, hTwo, heightTwo, depth) {
        super();
        this.joints.push(jointOne);
        this.heightOne = heightOne;
        this.joints.push(jointTwo);
        this.heightTwo = heightTwo;
        this.depthOne = jointOne.getDistance();
        this.depthTwo = jointTwo.getDistance();
        this.addParent(this.joints[0].root.owner);
        this.addParent(this.joints[1].root.owner);
        var geometryL = this.createGeom( jointOne.pointOne, this.depthOne, hOne, heightOne, Manager.thickness );
        var geometryR = this.createGeom( jointTwo.pointOne, this.depthTwo, hTwo, heightTwo, Manager.thickness );
        var matL = new THREE.MeshBasicMaterial( { color: 0x8888aa } );
        var matR = new THREE.MeshBasicMaterial( { color: 0x88aa77 } );
        var leftPane = new SurfaceMesh( geometryL, matL,"grab");
        this.surfaces.push(new Surface(this,leftPane));
        Manager.raycastList.push(this.surfaces[0].mesh);
        this.joints[0].leaf = this.surfaces[0];
        var rightPane = new SurfaceMesh( geometryR, matR ,"grab");
        this.surfaces.push(new Surface(this,rightPane));
        this.joints[1].leaf = this.surfaces[1];
        Manager.raycastList.push(this.surfaces[1].mesh);
        this.joints[0].pointA.add(this.surfaces[0].container);
        this.joints[1].pointA.add(this.surfaces[1].container);
        //division by 0 is cool: infinity which is handled nicely by atan(infinity)=Math.PI
        //this.joints[0].pointOne.x = 0.001;
        this.joints[0].pointA.rotation.y=-Math.atan((this.joints[0].pointTwo.y - this.joints[0].pointOne.y)/(this.joints[0].pointTwo.x - this.joints[0].pointOne.x))+Math.PI/2;
        this.joints[1].pointA.rotation.y=-Math.atan((this.joints[1].pointTwo.y - this.joints[1].pointOne.y)/(this.joints[1].pointTwo.x - this.joints[1].pointOne.x))+Math.PI/2;
        this.updatePoints();
    }
    move() {
        var guliAngle = this.joints[0].root.container.getWorldRotation().z - this.joints[1].root.container.getWorldRotation().z;
        var c = this.joints[0].pointOne.x;
        var a = this.joints[1].pointOne.x;
        var diag = Math.sqrt(c*c + a*a -2*a*c*Math.cos(guliAngle));
        var width = this.heightTwo;
        var height = this.heightOne;
        //clamp necessary: rounding error > 1 or < -1
        var angleOne = Math.acos(clamp((a*a - c*c - diag*diag)/(-2*c*diag),-1,1)) + Math.acos(clamp((height*height - width*width - diag*diag)/(-2*width*diag),-1,1));
        var angleTwo = Math.acos(clamp((c*c - a*a - diag*diag)/(-2*a*diag),-1,1)) + Math.acos(clamp((width*width - height*height - diag*diag)/(-2*height*diag),-1,1));
        this.surfaces[0].container.rotation.z = angleOne+Math.PI;
        this.surfaces[1].container.rotation.z =  angleTwo;
    
        var S = get_S(this.joints[0].pointA.getWorldPosition(),this.joints[0].pointB.getWorldPosition(),this.joints[1].pointA.getWorldPosition(),this.joints[1].pointB.getWorldPosition(),this.heightOne);

    }
    createGeom(point, depth, h, height, thickness) {
    
        var geometry = new THREE.Geometry();
    
        geometry.vertices.push(
            new THREE.Vector3( 0,  0, 0 ),
            new THREE.Vector3( 0, 0, 1 ),
            new THREE.Vector3(  height, 0, 1 ),
            new THREE.Vector3(  h, 0, 0 )
        );
    
        geometry.faces.push( new THREE.Face3( 0, 1, 2 ),new THREE.Face3( 0, 2, 1 ), new THREE.Face3( 0, 2, 3 ), new THREE.Face3( 0, 3, 2 ) );
    
        geometry.computeBoundingSphere();
    
        return geometry;
    };
    updatePoints(){
        var distOne = this.joints[0].getDistance();
        this.joints[0].cylinder.scale.set(1,distOne,1);
        this.surfaces[0].mesh.scale.z = distOne;
        var distTwo = this.joints[1].getDistance();
        this.joints[1].cylinder.scale.set(1,distTwo,1);
        this.surfaces[1].mesh.scale.z = distTwo;
    };
    delete(){
        console.log("deleting");
    }
}