import * as THREE from "three"
import {JointHandle} from "./jointHandle"
import {Surface} from "./surface"
import {JointInspector} from "./jointInspector"
import {Manager} from "misc/manager"
export class Joint {
    pointOne:THREE.Vector2;
    pointTwo:THREE.Vector2;
    pointA:THREE.Object3D;
    pointB:THREE.Object3D;
    sphereA:JointHandle;
    sphereB:JointHandle;
    cylinder:JointHandle;
    root:Surface;
    leaf:Surface;
    static raycastArray:Array<THREE.Mesh> = new Array<THREE.Mesh>();
    changed:JointHandle.handleType;
    subInspector:JointInspector;
    onchange:Array<Function> = new Array<Function>();

    constructor(surface:Surface, pointOne:THREE.Vector2, pointTwo:THREE.Vector2) {
        this.pointOne = pointOne;
        this.pointTwo = pointTwo;
        this.pointA = new THREE.Object3D();
        this.pointB = new THREE.Object3D();
        this.setSurface(surface);

        var geometry = new THREE.SphereGeometry( 0.025, 0.32, 0.32 );
        var material = new THREE.MeshBasicMaterial( {color: 0x222222} );
        this.sphereA = new JointHandle( geometry, material, this, "move",JointHandle.handleType.A );
        this.pointA.add( this.sphereA );
        this.sphereB = new JointHandle( geometry, material, this, "move",JointHandle.handleType.B );
        this.pointB.add( this.sphereB );

        var dist = pointOne.distanceTo(pointTwo);
        var geometryCyl = new THREE.CylinderGeometry( 0.0125, 0.0125, 1, 32 );
        geometryCyl.translate(0,-0.5,0);
        this.cylinder = new JointHandle( geometryCyl, material, this, "pointer", JointHandle.handleType.Center );
        this.cylinder.name = "cyl";
        this.cylinder.rotation.x=-Math.PI/2.0;
        this.cylinder.scale.set(1,dist,1);
        this.pointA.add( this.cylinder );

        this.subInspector = new JointInspector(this);

    }
    setSurface (surface) {
        this.root = surface;
        this.root.container.add(this.pointA);
        this.root.container.add(this.pointB);
        this.setPoints();
    }
    setPoints() {
        var pos = this.root.getLocalPosition(this.pointOne);
        this.pointA.position.set(pos.x,pos.y,pos.z);
        var pos2 = this.root.getLocalPosition(this.pointTwo);
        this.pointB.position.set(pos2.x,pos2.y,pos2.z);
        this.change();
        //update cylinder
    }
    updatePoints(type:JointHandle.handleType){
        this.changed = type;
        this.leaf.owner.updatePoints(this);
        this.change();
    }
    getDistance() {
        return this.pointA.position.distanceTo(this.pointB.position);
    }
    delete(){
        this.pointA.parent.remove(this.pointA);
        this.pointB.parent.remove(this.pointB);
    }
    get_SubInspector(){
        return this.subInspector;
    }
    change(){
        this.onchange.forEach(callback => {
            callback(this);
        })
        Manager.hasChanged = true;
    }
}