import * as THREE from "three"
import {Joint} from "elts/joint"
import {raycast} from "misc/utils"
import {Selectable} from "menu/selectable"
import {Iinspectable} from "menu/iinspectable"
import {Elt} from "elts/elt"

export class JointHandle extends THREE.Mesh implements Selectable,Iinspectable{
    cursor:string;
    jointOwner:Joint
    handleType:JointHandle.handleType;
    initialSelectionPoint:THREE.Vector3;

    constructor(geom:THREE.Geometry,mat:THREE.Material,jointOwner:Joint, cursor:string,handleType:JointHandle.handleType){
       super(geom,mat);
       this.jointOwner = jointOwner;
       this.cursor = cursor;
       this.handleType = handleType;

       Joint.raycastArray.push(this);
    }
    
    mousemove(event:MouseEvent){
        if(!this.jointOwner)
            return false;
        var intersections = raycast([this.jointOwner.root.mesh]);
        if(intersections.length > 0){
            var pos = this.parent.parent.worldToLocal(intersections[0].point).sub(this.initialSelectionPoint);
            this.parent.position.x = pos.x;
            this.parent.position.z = pos.z;
            this.jointOwner.updatePoints(this.handleType);
            return true;
        }
        return false;
    }

    selected(event:MouseEvent){
        var intersections = raycast([this]);
        this.initialSelectionPoint = this.parent.parent.worldToLocal(intersections[0].point).sub(this.parent.position);
    }

    get_Inspector(){
        return null;
    }
}
export module JointHandle{
    export enum handleType {A,Center,B};
}