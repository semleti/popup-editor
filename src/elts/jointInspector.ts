import {Joint} from "./joint"
import {JointHandle} from "./jointHandle"
export class JointInspector{
    htmlContainer:HTMLDivElement;
    constructor(joint:Joint){
        this.htmlContainer = document.createElement("div");
        var coord1 = document.createElement("input");
        coord1.type = "number";
        coord1.style.maxWidth = "70px";
        //rescale to better human usage, might want to do that globally
        coord1.value = (joint.pointA.position.x*10.0).toString();
        //workaraound hax to correctly update the rotations
        //TODO: proper fix correctly implemented!!!
        //TODO: round value getting from THREE to fit settings
        coord1.oninput = function(){if(coord1.value == "")return;joint.pointA.position.x = parseFloat(coord1.value.slice(0,coord1.value.indexOf(".")+6))/10.0;joint.updatePoints(JointHandle.handleType.Center);setTimeout(function(){joint.pointA.position.x = parseFloat(coord1.value)/10.0;joint.updatePoints(JointHandle.handleType.Center);},50);};
        this.htmlContainer.appendChild(coord1);
        
        var coord2 = document.createElement("input");
        coord2.type = "number";
        coord2.style.maxWidth = "70px";
        coord2.value = (joint.pointA.position.z*10).toString();
        coord2.oninput = function(){if(coord2.value == "")return;joint.pointA.position.z = parseFloat(coord2.value)/10.0;joint.updatePoints(JointHandle.handleType.Center);setTimeout(function(){joint.pointA.position.z = parseFloat(coord2.value)/10.0;joint.updatePoints(JointHandle.handleType.Center);},50);};
        this.htmlContainer.appendChild(coord2);

        var dist = document.createElement("input");
        dist.type = "number";
        dist.style.maxWidth = "70px";
        dist.value = (joint.getDistance()*10).toString();
        dist.oninput = function(){if(dist.value == "")return;joint.pointB.position.z = joint.pointA.position.z + parseFloat(dist.value)/10.0;joint.updatePoints(JointHandle.handleType.B);setTimeout(function(){joint.pointB.position.z = joint.pointA.position.z + parseFloat(dist.value)/10.0;joint.updatePoints(JointHandle.handleType.B)});};
        this.htmlContainer.appendChild(dist);
        
        //TODO: fill back in 0s if specified by user
        //TODO:prevent too big values BUT make it an option (both sides actually (nums after .))
        joint.onchange.push(function(joint:Joint){coord1.value = (Math.round(joint.pointA.position.x*10.0*100000)/100000).toString();coord2.value = (joint.pointA.position.z*10.0).toString();dist.value = (joint.getDistance()*10).toString()});
    }
}