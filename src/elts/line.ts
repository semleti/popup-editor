import * as THREE from "three"

export class Line{
    point:THREE.Vector3;
    vector:THREE.Vector3;
    constructor(point:THREE.Vector3,vector:THREE.Vector3){
        this.point=point;
        this.vector=vector;
    }
    distanceToPoint(point:THREE.Vector3):number{
        var dist = new THREE.Vector3().subVectors(this.point,point).cross(this.vector).length();
        return dist;
    }
}