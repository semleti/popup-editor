import * as THREE from "three"
import {Elt} from "./elt"
import {Line} from "./line"
import {Manager} from "misc/manager"
import {SurfaceMesh} from "elts/surfaceMesh"
import {Iinspectable} from "menu/iinspectable"
import {SurfaceInspector} from "./surfaceInspector"

export class Surface implements Iinspectable{
    container:THREE.Object3D;
    owner:Elt;
    mesh:SurfaceMesh;
    subInspector:SurfaceInspector;

    constructor(owner:Elt,mesh:SurfaceMesh){
        this.owner=owner;
        this.mesh=mesh;
        this.mesh.owner = this;
        this.container = new THREE.Object3D();
        this.container.add(this.mesh);
        this.subInspector = new SurfaceInspector(this);
    }
    getLocalPosition(point:THREE.Vector2) : THREE.Vector3{
        return new THREE.Vector3(point.x,0,point.y);
    }
    getNormal(){
        console.log("Please override getNormal!");
    }
    intersectSurface(other:Surface):Line{
        var n1 = this.mesh.up.clone().transformDirection( this.mesh.matrixWorld );
        var p1 = this.mesh.localToWorld(new THREE.Vector3(0,0,0));
        var d1 = p1.x*n1.x + p1.y*n1.y + p1.z*n1.z;
        var n2 = other.mesh.up.clone().transformDirection( other.mesh.matrixWorld );
        var p2 = other.mesh.localToWorld(new THREE.Vector3(0,0,0));
        var d2 = p2.x*n2.x + p2.y*n2.y + p2.z*n2.z;
        var u = new THREE.Vector3().crossVectors(n2,n1).normalize();
        var Pu = new THREE.Vector3(0,0,0);
        if(Math.abs(u.z) > Math.abs(u.y) && Math.abs(u.z) > Math.abs(u.x))
        {
            Pu.z = 0;
            Pu.x = (n1.y*d2-n2.y*d1)/(n1.x*n2.y-n2.x*n1.y);
            Pu.y = (n2.x*d1-n1.x*d2)/(n1.x*n2.y-n2.x*n1.y);
        }
        else if(Math.abs(u.y) > Math.abs(u.x))
        {
            Pu.y = 0;
            Pu.x = (n1.z*d2-n2.z*d1)/(n1.x*n2.z-n2.x*n1.z);
            Pu.z = (n2.x*d1-n1.x*d2)/(n1.x*n2.z-n2.x*n1.z);
        }
        else
        {
            Pu.x = 0;
            Pu.y = (n1.z*d2-n2.z*d1)/(n1.y*n2.z-n2.y*n1.z);
            Pu.z = (n2.y*d1-n1.y*d2)/(n1.y*n2.z-n2.y*n1.z);
        }
        return new Line(Pu,u);
    }
    delete(){
        
    }
    get_Inspector():HTMLElement{
        return this.owner.get_Inspector();
    }
    get_SubInspector(){
        return new SurfaceInspector(this);
    }
}