import * as THREE from "three"
import {ColorPicker} from "menu/colorPicker"
import {Manager} from "misc/manager"
import {Surface} from "./surface"
export class SurfaceInspector{
    colorPicker:ColorPicker;
    htmlContainer:HTMLElement;
    constructor(surface:Surface){
        this.htmlContainer = document.createElement("div");
        this.colorPicker = new ColorPicker();
        this.htmlContainer.appendChild(this.colorPicker.domElement);
        var t = this;
        var func = function(event){(<THREE.MeshBasicMaterial>surface.mesh.material).color.setStyle(t.colorPicker.color);Manager.isDirty = true;};
        this.colorPicker.colorCallback = func;
        this.colorPicker.setColor((<any>surface.mesh.material).color.getHexString());
    }
}