import * as THREE from "three"
import {Elt} from "elts/elt"
import {Surface} from "./surface"
import {Iinspectable} from "menu/iinspectable"

export class SurfaceMesh extends THREE.Mesh implements Iinspectable{
    cursor:string;
    owner:Surface;

    constructor(geom:THREE.Geometry,mat:THREE.Material,cursor:string="default"){
       super(geom,mat);
       this.cursor = cursor;
    }

    get_Inspector():HTMLElement{
        return this.owner.get_Inspector();
    }
}