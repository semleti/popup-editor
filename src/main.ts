import * as THREE from 'three'
import {Manager} from "misc/manager"
import {JointHandle} from "elts/jointHandle"
import {raycast,clamp} from "misc/utils"
import {Joint} from "elts/joint"
import {EltParallel} from "./elts/eltParallel"
import {EltParallelogram} from "./elts/eltParallelogram"
import {EltVFold} from "./elts/eltVFold"
import {Selectable} from "menu/selectable"


Manager.build();

function wheelRotation(e) {
    if(e.deltaY > 0)
        Manager.toMove += Math.PI / 20.0;
    else if(e.deltaY < 0)
        Manager.toMove -= Math.PI / 20.0;
}
Manager.renderer.domElement.addEventListener("wheel", wheelRotation, {passive:true});


window.oncontextmenu = function(event){
    //event.preventDefault();
    return false;
};

//TODO: prevent del from triggering when inside input
window.onkeydown = function(event){
    if(Manager.menuBar.toolBar.selectedTool.keydown(event))
    {
        event.preventDefault();
    }
}

Manager.renderer.domElement.ondblclick = function(event){
    event.preventDefault();
    console.log("prevented");
}



Manager.renderer.domElement.onmousedown = function(event){
    Manager.menuBar.toolBar.selectedTool.mouseDown(event);
};

Manager.renderer.domElement.onmouseup = function(event){
    Manager.menuBar.toolBar.selectedTool.mouseUp(event);
};

Manager.renderer.domElement.onmousemove = function( event:MouseEvent) {
	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components
	Manager.mouse.x = ( event.offsetX / Manager.renderer.domElement.width ) * 2 - 1;
    Manager.mouse.y = - ( event.offsetY / Manager.renderer.domElement.height ) * 2 + 1;
    
    if(!Manager.menuBar.toolBar.selectedTool.mouseMove(event))
    {
        if(event.buttons %2 == 1)
        {
            Manager.camera.move(event);
            Manager.isDirty = true;
        }
    }   
}



window.onresize = function(event){
    Manager.camera.aspect = window.innerWidth/window.innerHeight;
    Manager.camera.updateProjectionMatrix();
    Manager.renderer.setSize( window.innerWidth - 200, window.innerHeight - 0 );
    Manager.isDirty = true;
}


function animate() {
    requestAnimationFrame( animate );
    cannonUpdate();
    var rot = Manager.pageLeft.surfaces[0].container.rotation.z;
    if(Manager.toMove > 0.1)
    {
    	rot += 0.1;
        Manager.toMove -= 0.1;
    }
    else if(Manager.toMove < -0.1)
    {
        rot -= 0.1;
        Manager.toMove += 0.1;
    }
    else
        Manager.toMove = 0;
    rot = clamp(rot,0.001,Math.PI-0.001);
    if(Manager.pageLeft.surfaces[0].container.rotation.z != rot)
    {
        Manager.pageLeft.surfaces[0].container.rotation.z = rot;
        Manager.hasChanged = true;
    }
    if(Manager.hasChanged)
    {
        Manager.pageLeft.tellChildrenReady();
        Manager.pageRight.tellChildrenReady();
        Manager.isDirty = true;
    }
    var intersects = raycast(Joint.raycastArray);
    if(intersects.length>0)
    {
        if(intersects[0].object instanceof JointHandle)
        {
            document.body.style.cursor = (intersects[0].object as JointHandle).cursor;
        }
        else
            document.body.style.cursor = "default";
    }
    else
    {
        document.body.style.cursor = "default";
    }
    /*for ( var i = 0; i < intersects.length && i < 1; i++ ) {

        //intersects[ i ].object.material.color.set( 0xff0000 );
        var pos = center.worldToLocal(intersects[i].point);
        sphere.position.set(pos.x,pos.y,pos.z);
    }*/
    if(Manager.isDirty)
        Manager.renderer.render(Manager.scene, Manager.camera);
    Manager.isDirty = false;
    Manager.hasChanged = false;
}

var par = new EltParallel(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.3,0),new THREE.Vector2(0.3,0.1)),new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.2,0), new THREE.Vector2(0.2,0.2)),0.5,0.6,0x882222);
/*var par2 = new EltParallel(new Joint(par.surfaces[1],new THREE.Vector2(0.5,0.1),new THREE.Vector2(0.5,0.2)),new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.4,0), new THREE.Vector2(0.4,0.2)),0.2,0.5,0x222288);
var par3 = new EltParallel(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.5,0),new THREE.Vector2(0.5,0.1)),new Joint(par.surfaces[0],new THREE.Vector2(0.5,0), new THREE.Vector2(0.5,0.2)),0.5,0.2,0x228822);
var par4 = new EltParallel(new Joint(par.surfaces[1],new THREE.Vector2(0.6,0.1),new THREE.Vector2(0.6,0.2)),new Joint(par2.surfaces[0],new THREE.Vector2(0.1,0), new THREE.Vector2(0.1,0.2)),0.1,0.1,0x888822);
*/var para = new EltParallelogram(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.5,0.5),new THREE.Vector2(0.5,0.7)),new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.2,0.5),new THREE.Vector2(0.2,0.7)),0x228888);
/*//var para2 = new EltParallelogram(new AttachmentPoint(pageLeft,new THREE.Vector2(0.5,0.5)),new AttachmentPoint(pageRight,new THREE.Vector2(0.2,0.5)),0.5);
var vfold = new EltVFold(new Joint(Manager.pageLeft.surfaces[0],new THREE.Vector2(0.1,0.1),new THREE.Vector2(0.5,0.5)),0.1,0.72,new Joint(Manager.pageRight.surfaces[0],new THREE.Vector2(0.1,0.1),new THREE.Vector2(0.5,0.5)),0.1,0.8,0.3);
*/



var world = new CANNON.World();
var geometry = new THREE.BoxGeometry( 0.2, 0.00001, 0.2 );
var timeStep = 1/60.0;
var number = 20;

var meshes = [];
var bodies = new Array<CANNON.Body>();
var springs = [];
var hinges = new Array<CANNON.HingeConstraint>();
var p2ps = new Array<CANNON.PointToPointConstraint>();

function cannonTest(){
    world.gravity.set(0,-9.82*0.01,0);
    world.broadphase = new CANNON.NaiveBroadphase();
    world.solver.iterations = 1;
    var shape = new CANNON.Box(new CANNON.Vec3(0.2,0.00001,0.2));
    var mass = 1;


    for(var i = 0; i < number; i++)
    {
        var material = new THREE.MeshBasicMaterial( { color: i*150, wireframe: true } );
        var mesh = new THREE.Mesh( geometry, material );
        Manager.center.add( mesh );
        meshes.push(mesh);
    }

    for(var i = 0; i < number; i++)
    {
        var body = new CANNON.Body({
        mass: i != 0 && i < (number-1) ? 1:0
        });
        body.addShape(shape);
        body.angularDamping = 0.5;
        body.position.set((i-number/2.0)*0.2,0,0);
        body.linearDamping = 0.5;
        body.collisionFilterGroup = 2;
        body.collisionFilterMask = 2;
        world.addBody(body);
        bodies.push(body);
    }
    //bodies[bodies.length-2].mass = 0;
    //bodies[bodies.length-1].position.set(1,0,0);
    //bodies[bodies.length-2].sleep();
    console.log(bodies[bodies.length-1].mass);

    /*for(var i = 1; i < number; i++)
    {
        var spring = new CANNON.Spring({
            restLength : 0.1,
            stiffness : 1000,
            damping : 10000,
            localAnchorA: new CANNON.Vec3(-0.1,0,0),
            localAnchorB: new CANNON.Vec3(0.1,0,0),
        });
        spring.bodyA = bodies[i-1];
        spring.bodyB = bodies[i];
        springs.push(spring);
    }*/

    /*for(var i = 1; i < number; i++)
    {
        var hinge = new CANNON.HingeConstraint(bodies[i-1],bodies[i],{
            pivotA: new CANNON.Vec3(0.1,0,0),
            axisA: new CANNON.Vec3(0,0,1),
            pivotB: new CANNON.Vec3(-0.1,0,0),
            axisB: new CANNON.Vec3(0,0,1),
            maxForce: i != 0 && i !=(bodies.length-1) ? 100000000:1,
        });
        hinges.push(hinge);
        world.addConstraint(hinge);
    }*/
    bodies[0].collisionFilterGroup=1;
    bodies[bodies.length-1].collisionFilterGroup=1;
    for(var i = 1; i < number; i++)
    {
        var p2p1 = new CANNON.PointToPointConstraint(bodies[i-1],new CANNON.Vec3(0.1,0,-0.1),bodies[i],new CANNON.Vec3(-0.1,0,-0.1),100);
        var p2p2 = new CANNON.PointToPointConstraint(bodies[i-1],new CANNON.Vec3(0.1,0,0.1),bodies[i],new CANNON.Vec3(-0.1,0,0.1),100);
        var dist = 
        p2ps.push(p2p1);
        p2ps.push(p2p2);
        world.addConstraint(p2p1);
        world.addConstraint(p2p2);
    }


    
    // Compute the force after each step
    world.addEventListener("preStep",function(event){
        springs.forEach(spring => {
            spring.applyForce();
        });
    });

}
cannonTest();
var cannonCounter = 600;
function cannonUpdate(){
    if(Manager.hasChanged)
    {
        cannonCounter = 600;
    }
    if(cannonCounter > 0)
    {
        cannonCounter--;
        for(var i = 0; i < 60; i++)
        {
            world.step(timeStep);
        }
    }
    // Copy coordinates from Cannon.js to Three.js
    for(var i = 0; i < number; i++)
    {
        meshes[i].position.copy(bodies[i].position);
        meshes[i].quaternion.copy(bodies[i].quaternion);
    }
    Manager.isDirty = true;
}

window.onkeydown = function(event){
    var mov = 0;
    if(event.key == "a")
    {
        mov = 0.05;
    }if(event.key == "z")
    {
        mov = -0.05;     
    }
    if(mov != 0)
    {
        for(var i = 0; i < 1; i++)
        {
            bodies[0].position.x += mov/1.0;
            bodies[bodies.length-1].position.x -= mov/1.0;
            world.step(timeStep);
        }
        Manager.hasChanged = true;   
    }
}





animate();