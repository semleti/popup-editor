import {Manager} from "misc/manager"
export class ColorPicker{
    domElement:HTMLInputElement;
    color:string;
    colorCallback;
    constructor(){

        this.domElement = document.createElement("input");
        this.domElement.type = "color";
        var t = this;
        this.domElement.oninput = function(){t.oninput();};
    }
    setColor(color){
        this.domElement.value = "#" + color;
    }
    oninput(){
        this.color = this.domElement.value;
        this.colorCallback();
    }

}