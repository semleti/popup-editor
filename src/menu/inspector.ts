import * as THREE from "three"
import {Manager} from "misc/manager"
import {Selectable} from "./selectable"
import {Elt} from "elts/elt"
import {Joint} from "elts/joint"
import {JointHandle} from "elts/jointHandle"
import {Iinspectable} from "./iinspectable"

export class Inspector{
    htmlContainer:HTMLDivElement;
    /*htmlHeader:HTMLParagraphElement;
    colorPicker:ColorPicker;
    htmlSurfaceContainer:HTMLDivElement;
    htmlJointContainer:HTMLDivElement;*/
    constructor(){
        this.htmlContainer = <HTMLDivElement>document.createElement("div");

    }
    select(object:THREE.Object3D, event:MouseEvent){
        Manager.selectedObject = object ;
        var select = <any>Manager.selectedObject;
        if((<Selectable>select).selected){
            (select as Selectable).selected(event);
        }
        if(select instanceof JointHandle)
        {
            if(select.jointOwner)
                select = select.jointOwner.leaf;
        }
        if(select && select.get_Inspector){
            this.htmlContainer.innerHTML = "";
            var inspector = (<any>(<Iinspectable>select).get_Inspector());
            if(inspector)
            {
                this.htmlContainer.appendChild(inspector);
                return true;
            }
            return true;
        }
        return false;
    }
    selectNothing(){
        Manager.selectedObject = null;
        this.htmlContainer.innerHTML = "";
    }
}