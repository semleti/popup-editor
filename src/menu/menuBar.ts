import {Inspector} from "./inspector"
import {ToolBar} from "./toolBar"

export class MenuBar{
    htmlContainer:HTMLDivElement;
    inspector:Inspector;
    toolBar:ToolBar;
    constructor(){
        this.htmlContainer = <HTMLDivElement>document.getElementById("menuBar");
        this.htmlContainer.style.maxWidth = "200px";
        this.inspector = new Inspector();
        this.htmlContainer.appendChild(this.inspector.htmlContainer);
        this.toolBar = new ToolBar();
        this.htmlContainer.appendChild(this.toolBar.htmlContainer);
    }
}