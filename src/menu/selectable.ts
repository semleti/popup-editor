export abstract class Selectable{
    abstract selected(event:MouseEvent);
}