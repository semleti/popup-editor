import {Tool} from "tools/tool"
import {ToolDefault} from "tools/toolDefault"
import {ToolAdd} from "tools/toolAdd"
export class ToolBar{
    htmlContainer:HTMLDivElement;
    tools:Array<Tool> = new Array<Tool>();
    toolButtons:Array<HTMLButtonElement> = new Array<HTMLButtonElement>();
    toolButtonsContainer:HTMLDivElement;
    toolContainer:HTMLDivElement;
    selectedTool:Tool;
    selectedToolIndex:number=0;
    constructor(){
        this.htmlContainer = document.createElement("div");
        this.htmlContainer.style.border = "2px";
        this.htmlContainer.style.borderColor = "blue";
        this.htmlContainer.style.borderStyle = "solid";

        this.toolButtonsContainer = document.createElement("div");
        this.htmlContainer.appendChild(this.toolButtonsContainer);

        this.toolContainer = document.createElement("div");
        this.htmlContainer.appendChild(this.toolContainer);

        this.addTool(new ToolDefault());
        this.addTool(new ToolAdd());
        this.selectTool(0);
    }
    addTool(tool:Tool){
        var but = document.createElement("button");
        var index = this.tools.length;
        this.tools.push(tool);
        var t = this;
        but.onclick = function(){t.selectTool(index)};
        but.appendChild(tool.get_ButtonWidget());
        this.toolButtonsContainer.appendChild(but);
        this.toolButtons.push(but);
    }
    selectTool(index:number){
        this.toolButtons[this.selectedToolIndex].style.backgroundColor = "buttonface";
        try{
        this.toolContainer.removeChild(this.tools[this.selectedToolIndex].get_ToolWidget());
        }catch(e){}
        this.selectedTool = this.tools[index];
        this.selectedToolIndex = index;
        this.toolButtons[index].style.backgroundColor = "white";
        this.toolContainer.appendChild(this.tools[this.selectedToolIndex].get_ToolWidget());
    }
}