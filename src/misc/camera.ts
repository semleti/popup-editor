import * as THREE from "three"
import {clamp} from "./utils"
import {Manager} from "./manager"

export class Camera extends THREE.PerspectiveCamera{
    pitch:number = 0;
    yaw:number = 0;
    constructor(fov:number,aspect:number,near:number,far:number){
        super(fov,aspect,near,far);
    }
    move(event:MouseEvent){
        this.pitch += event.movementY / 100.0;
        this.yaw += event.movementX / 100.0;
        this.pitch = clamp(this.pitch,0,Math.PI/2.0);
        this.setCameraPos(this.pitch,this.yaw);
    }
    setCameraPos(pitch:number,yaw:number){
        this.rotation.x = - pitch;
        this.position.y = Math.sin(pitch) * 3;
        this.position.z =  Math.cos(pitch) * 3;
        Manager.center.rotation.y = yaw;
    }
}