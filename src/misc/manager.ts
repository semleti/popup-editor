import * as THREE from "three"
import {Camera} from "./camera"
import {EltPage} from "elts/eltPage"
import {Skybox} from "./skybox"
import {MenuBar} from "menu/menuBar"

export class Manager{
    static center:THREE.Object3D;
    static raycastList:Array<THREE.Mesh>;
    static scene:THREE.Scene;
    static camera:Camera;
    static renderer:THREE.WebGLRenderer;
    static mouse:THREE.Vector2;
    static selectedObject:THREE.Object3D;
    static pageLeft:EltPage;
    static pageRight:EltPage;
    static pageThickness:number;
    static thickness:number;
    static toMove:number;
    static raycaster:THREE.Raycaster;
    static skybox:Skybox;
    static isDirty:boolean = true;
    static hasChanged:boolean = true;
    static menuBar:MenuBar;

    static build() {
        Manager.scene = new THREE.Scene();
        Manager.camera = new Camera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
        Manager.camera.position.z = -3;
        
        Manager.renderer = new THREE.WebGLRenderer({antialias:true});
        Manager.renderer.setSize( window.innerWidth - 200, window.innerHeight - 0 );
        document.body.appendChild( Manager.renderer.domElement );
        
        Manager.center = new THREE.Object3D();
        Manager.scene.add( Manager.center );

        Manager.raycastList = new Array<THREE.Mesh>();
        Manager.selectedObject = null;

        Manager.mouse = new THREE.Vector2(-5,-5);

        Manager.pageThickness = 0.05;
        Manager.pageLeft = new EltPage(0x888877,1,1,Manager.pageThickness,1);
        Manager.pageRight = new EltPage(0x555544,1,1,Manager.pageThickness,-1);
        
        Manager.thickness = 0.01;

        Manager.toMove = 0;

        Manager.raycaster = new THREE.Raycaster();

        Manager.skybox = new Skybox();
        Manager.camera.setCameraPos(0,0);

        Manager.menuBar = new MenuBar();
    }
}