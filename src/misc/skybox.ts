import * as THREE from "three"
import {Manager} from "./manager"
export class Skybox {
     mesh : THREE.Mesh;
     loadedNum:number = 0;
    constructor(){
        var t = this;
        var materialArray = [];
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_ft.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_bk.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_up.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_dn.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_rt.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        materialArray.push(new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load( 'resources/sor_lake1/lake1_lf.jpg' ,function(){t.loaded();}), side: THREE.DoubleSide }));
        for (var i = 0; i < 6; i++)
        materialArray[i].side = THREE.BackSide;
        var skyboxMaterial = materialArray;
        var skyboxGeom = new THREE.CubeGeometry( 25, 25, 25, 1, 1, 1 );
        this.mesh = new THREE.Mesh( skyboxGeom, skyboxMaterial );
        Manager.center.add(this.mesh);
    }
    loaded(){
        this.loadedNum++;
        if(this.loadedNum == 6)
            Manager.isDirty = true;
    }
}