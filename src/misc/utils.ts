import * as THREE from "three"
import {Manager} from "./manager"

export function clamp(val,min,max){
    return Math.min(max,Math.max(min,val));
}

export function get_line_intersection(p0_x, p0_y, p1_x, p1_y,
    p2_x, p2_y, p3_x, p3_y)
{
    var s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    var s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        return new THREE.Vector2(p0_x + (t * s1_x),p0_y + (t * s1_y));
    }

    return 0; // No collision
}



export function get_S(A1,B1,A2,B2,distance) {
   var d1x = B1.x-A1.x;
   var d1y = B1.y-A1.y;
   var d1z = B1.z-A1.z;
   var d2x = B2.x-A2.x;
   var d2y = B2.y-A2.y;
   var d2z = B2.z-A2.z;
   /*
   //  SAz = A1.z + ((A1.x - SAx)*d1x+(A1.y-SAy)*d1y)/d1z;
   //  SAz=  h    + ((i    - b  )*j  +(k   -SAy)*l  )/m
   //
   //  SAy = A2.y + ((A2.x - SAx)*d2x+(A2.z-SAz)*d2z)/d2y;
   //  SAy = ((A2.x-SAx)*d2x+A2.y*d2y+d2z*(A2.z-(A1.z+(A1.x-SAx)*d1x+A1.y*d1y)/d1z))/(d2y+(d1y*d2z)/d1z);
   //  SAy = ((a   - b )*c  +d   *e  +f  *(g   -(h   +(i   -b  )*j  +k   *l  )/m  ))/(e  +(l  *f  )/m  );
   //  SAy = (m  *(a   *c  -b  *c  +d   *e  )-f  *(-b  *j  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
   //var SAy = (d1z*(A2.x*d2x-SAx*d2x+A2.y*d2y)-d2z*(-SAx*d1x-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y);

   //  SAy = (m  *(a   *c    +d   *e  )-b*c*m+f*b*j-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
   //  SAy = (m  *(a   *c    +d   *e  )+b(-c*m+f*j)-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
   //  SAy = (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ) +b(-c*m+f*j)/(e*m+f*l)
   //  SAy =  b*(-c*m+f*j)/(e*m+f*l) + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
   //  SAy =  b *(-c  *m  +f  *j)  /(e  *m  +f  *l  ) + (m  *(a   *c   +d   *e  )-f  *(-g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  )
   //SAy   = SAx*(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y) + (d1z*(A2.x*d2x +A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y);
   //SAy     = SAx*KY + CY;
   */
   var KY = (-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y);
   var CY = (d1z*(A2.x*d2x +A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y);

   /*
   //var SAz = A1.z + ((A1.x - SAx)*d1x+(A1.y-SAx*(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y) + (d1z*(A2.x*d2x +A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y))*d1y)/d1z;
   //  SAz=  h    + ((i    - b  )*j  +(k   -b*(-c*m+f*j)/(e*m+f*l) + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l  )/m
   //SAz=  h    + (i*j    - b*j  +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l -b*(-c*m+f*j)/(e*m+f*l)*l )/m
   //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l- b*j -b*(-c*m+f*j)/(e*m+f*l)*l )/m
   //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l - b*(j+(-c*m+f*j)/(e*m+f*l)*l) )/m
   //SAz=  h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l )/m - b*(j+(-c*m+f*j)/(e*m+f*l)*l)/m
   //SAz=  - b*(j+(-c*m+f*j)/(e*m+f*l)*l)/m + h    + (i*j   +(k + (m  *(a   *c    +d   *e  )-f  *(  -g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l )/m;
   //SAz=-b  *(j  +(-c  *m  +f  *j  )/(e  *m  +f  *l  )*l  )/m   + h   +(i   *j  +(k   +(m  *(a   *c  +d   *e  )-f  *(-g   *m  +h   +i   *j  +k   *l  ))/(e  *m   +f *l  ))*l  )/m;
   //SAz=-SAx*(d1x+(-d2x*d1z+d2z*d1x)/(d2y*d1z+d2z*d1y)*d1y)/d1z + A1.z+(A1.x*d1x+(A1.y+(d1z*(A2.x*d2x+A2.y*d2y)-d2z*(-A2.z*d1z+A1.z+A1.x*d1x+A1.y*d1y))/(d2y*d1z+d2z*d1y))*d1y)/d1z;
   //  SAz=  h    + ((i    - b  )*j  +(k   -SAy)*l  )/m
   //  SAz=  h    + ((i    - b  )*j  +(k   -b*KY+CY)*l  )/m
   //  SAz=  h    + (i*j   - b*j -b*KY*l +(k   +CY)*l  )/m
   //  SAz=  h    + (i*j   - b*(j+KY*l) +(k   +CY)*l  )/m
   //  SAz=  h    + (i*j    +(k   +CY)*l  )/m - b*(j+KY*l)/m
   //  SAz= - b*(j+KY*l)/m + h    + (i*j    +(k   +CY)*l  )/m
   //  SAz= - b*(j  +KY*l  )/m   + h    + (i   *j    +(k   +CY)*l  )/m
   //  SAz= - b*(d1x+KY*d1y)/d1z + A1.z + (A1.x*d1x  +(A1.y+CY)*d1y)/d1z
   //SAz=SAx*KZ+CZ;
   */
   var KZ = -(d1x+KY*d1y)/d1z;
   var CZ = A1.z + (A1.x*d1x  +(A1.y+CY)*d1y)/d1z;

   //(A1.x-SAx)*(A1.x-SAx)          +  (A1.y-SAy)*(A1.y-SAy)         +  (A1.z-SAz)*(A1.z-SAz)
   //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  SAy*SAy-2*SAy*A1.y+A1.y*A1.y  +  SAz*SAz-2*SAz*A1.z+A1.z*A1.z
   //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  (SAx*KY+CY)*(SAx*KY+CY)-2*(SAx*KY+CY)*A1.y+A1.y*A1.y  +  (SAx*KZ+CZ)*(SAx*KZ+CZ)-2*(SAx*KZ+CZ)*A1.z+A1.z*A1.z
   //SAx*SAx-2*SAx*A1.x+A1.x*A1.x   +  SAx*SAx*KY*KY + SAx*2*KY*CY + CY*CY - SAx*2*KY*A1.y + A1.y*A1.y-2*CY*A1.y+A1.y*A1.y  +  SAx*SAx*KZ*KZ + SAx*2*KZ*CZ + CZ*CZ - SAx*2*KZ*A1.z + A1.z*A1.z-2*CZ*A1.z+A1.z*A1.z
   //SAx*SAx*(1+KY*KY+KZ*KZ)  +  SAx*(-2*A1.x+2*KY*CY-2*KY*A1.y+2*KZ*CZ-2*KZ*A1.z)  +   A1.x*A1.x + CY*CY  + A1.y*A1.y-2*CY*A1.y+A1.y*A1.y + CZ*CZ  + A1.z*A1.z-2*CZ*A1.z+A1.z*A1.z - h1*h1 == 0
   //SAx*SAx*a  +  SAx*b  +   A1.x*A1.x + CY*CY  + c == 0
   var a = 1+KY*KY+KZ*KZ;
   var b = -2*A1.x+2*KY*CY-2*KY*A1.y+2*KZ*CZ-2*KZ*A1.z;
   var c = A1.x*A1.x + CY*CY  + A1.y*A1.y-2*CY*A1.y+A1.y*A1.y + CZ*CZ  + A1.z*A1.z-2*CZ*A1.z+A1.z*A1.z - distance*distance;
   var delta = b*b-4*a*c;
   var SAx = 0;
   if(delta < 0)
       console.log("no corresponding point");
   else if(delta == 0)
       SAx = -b*0.5/a;
   else
       SAx = (-b+Math.sqrt(delta))*0.5/a;

   var SAy = SAx*KY + CY;
   var SAz = SAx*KZ + CZ;

   return new THREE.Vector3(SAx,SAy,SAz);
}

export function raycast(objects) {
	// update the picking ray with the camera and mouse position
	Manager.raycaster.setFromCamera( Manager.mouse, Manager.camera );

	// calculate objects intersecting the picking ray
	return Manager.raycaster.intersectObjects( objects );


}
