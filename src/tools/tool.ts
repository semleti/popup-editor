export abstract class Tool{
    buttonWidget:HTMLElement;
    toolWidget:HTMLElement;
    get_ButtonWidget():HTMLElement{
        return this.buttonWidget;
    }
    get_ToolWidget():HTMLElement{
        return this.toolWidget;
    }
    abstract mouseDown(event:MouseEvent):boolean;
    abstract mouseUp(event:MouseEvent):boolean;
    abstract mouseMove(event:MouseEvent):boolean;
    abstract keydown(event:KeyboardEvent):boolean;
}