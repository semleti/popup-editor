import {Tool} from "./tool"
import {EltAdd} from "elts/eltAdd";
import {EltPageAdd} from "elts/eltPageAdd"
import {EltParallelAdd} from "elts/eltParallelAdd"
import {EltParallelogramAdd} from "elts/eltParallelogramAdd"

export class ToolAdd extends Tool{
    adders:Array<EltAdd> = new Array<EltAdd>();
    buttons:Array<HTMLButtonElement> = new Array<HTMLButtonElement>();
    selectedAdder:number = 0;
    constructor(){
        super();
        this.buttonWidget = document.createElement("p");
        this.buttonWidget.textContent = "add";
        this.toolWidget = document.createElement("div");

        this.addAdder(new EltPageAdd());
        this.addAdder(new EltParallelAdd());
        this.addAdder(new EltParallelogramAdd());
    }
    addAdder(elt:EltAdd){
        var index = this.adders.length;
        this.adders.push(elt);
        var but = document.createElement("button");
        but.appendChild(elt.icon);
        var t = this;
        but.onclick = function(){t.selectedAdder = index};
        this.buttons.push(but);
        this.toolWidget.appendChild(but);
    }
    mouseUp(event:MouseEvent){return this.adders[this.selectedAdder].mouseUp(event);}
    mouseDown(event:MouseEvent){return this.adders[this.selectedAdder].mouseDown(event);}
    mouseMove(event:MouseEvent){return this.adders[this.selectedAdder].mouseMove(event);}
    keydown(event:KeyboardEvent){return this.adders[this.selectedAdder].keydown(event);}
}