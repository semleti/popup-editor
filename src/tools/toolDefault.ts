import {Tool} from "./tool"
import {Manager} from "misc/manager"
import {Joint} from "elts/joint"
import {raycast} from "misc/utils"
import {JointHandle} from "elts/jointHandle"
import {Elt} from "elts/elt"

export class ToolDefault extends Tool{
    constructor(){
        super();
        this.buttonWidget = document.createElement("p");
        this.buttonWidget.textContent = "def";
        this.toolWidget = document.createElement("p");
        this.toolWidget.textContent = "select/move stuff";
    }
    mouseDown(event:MouseEvent){
        if(event.buttons %2 == 1) {
            var arr = new Array();
            arr = Manager.raycastList.concat(Joint.raycastArray);
            var intersects = raycast(arr);
            if(intersects.length > 0 && Manager.menuBar.inspector.select(intersects[0].object, event))
            {
                console.log("inspecting");
                return true;
            }
            else
                Manager.menuBar.inspector.selectNothing();
        }
        return false;
    }
    mouseUp(event:MouseEvent){
        return false;
    }
    mouseMove(event:MouseEvent){
        if(event.buttons %2 == 1)
        {
            if(Manager.selectedObject && Manager.selectedObject instanceof JointHandle)
            {
                (Manager.selectedObject as JointHandle).mousemove(event);
                return true;
            }
        }
        return false;
    }
    keydown(event:KeyboardEvent){
        //delete
        if(event.keyCode == 46 && Manager.selectedObject)
        {
            if((<any>Manager.selectedObject).owner)
                (<any>Manager.selectedObject).owner.owner.delete();
            else if ((<any>Manager.selectedObject).jointOwner)
                (<any>Manager.selectedObject).jointOwner.leaf.owner.delete();            
            return true;
        }
        return false;
    }
}